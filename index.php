<?php header('Content-type: text/html; charset=utf-8'); ?>
<!doctype html>
<html amp lang="en">
<head>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<title>Hello, AMP</title>
<link rel="canonical" href="http://localhost/amp.php">
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "NewsArticle",
		"headline": "Open-source framework for publishing content",
		"datePublished": "2015-10-07T12:02:41Z",
		"image": [
		  "logo.jpg"
		]
	}
</script>
<style amp-boilerplate>

	@import 'css-amp.less';

	body
	{
		-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;
		-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;
		-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;
		animation:-amp-start 8s steps(1,end) 0s 1 normal both;
	}

	@-webkit-keyframes -amp-start
	{
		from {visibility:hidden} to {visibility:visible}
	}
	@-moz-keyframes -amp-start
	{
		from {visibility:hidden} to {visibility:visible}
	}
	@-ms-keyframes -amp-start
	{
		from {visibility:hidden} to {visibility:visible}
	}
	@-o-keyframes -amp-start
	{
		from {visibility:hidden} to {visibility:visible}
	}
	@keyframes -amp-start
	{
		from {visibility:hidden} to {visibility:visible}
	}
</style>
<noscript>
	<style amp-boilerplate>
		body {-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}
	</style>
</noscript>
</head>

<body>
<h1>Bem-vindo à internet móvel</h1>
<amp-img
	src="https://fakeimg.pl/356x224/333/fff/?text=356x224"
	width="356"
	height="224"
	alt="Welcome"
>
</amp-img>
</body>
</html>