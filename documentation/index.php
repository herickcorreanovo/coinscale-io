<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Documentação COINSCALE.IO</title>
	<!-- Última versão CSS compilada e minificada -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://use.fontawesome.com/9f6cfc4141.js"></script>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col- col-sm- col-md- col-lg- col-xl-">
			<?php
				$exchangesAdicionadas = array(
					'mercado-bitcoin',
					'tembtc',
					'negociecoins',
					'3xbit',
					'binance',
					'poloniex',
					'bitfinex',
				);
			?>
			<?php $url = 'https://'.$_SERVER['HTTP_HOST']; ?>
			
			<div class="page-header">
				<h1>Documentação coinscale.io</h1>
				<p><strong>Base endpoint:</strong> <?php echo $url.'/api'; ?></p>
			</div>

			<div class="panel panel-primary">
				<div class="panel-heading">Backend</div>
				<table class="table table-striped">
					<thead class="thead-dark">
						<tr>
							<th scope="col">Tipo</th>
							<th scope="col">URL</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Informações e Taxas</td>
							<td><a href="<?php echo $url; ?>/backend/exchanges/" target="_blank">/backend/exchanges/</a></td>
						</tr>
						<tr>
							<td>Notas de Acesso Similarweb</td>
							<td><a href="<?php echo $url; ?>/backend/similarweb/" target="_blank">/backend/similarweb/</a></td>
						</tr>
						<tr>
							<td>JSON Similarweb Nacionais</td>
							<td>
								<a href="<?php echo $url; ?>/backend/json/comparador-de-acessos-nacionais/?token=FxQYhUmg6XpvtN5NsQ9PBZeP1rvKBiai" target="_blank">
									/backend/json/comparador-de-acessos-nacionais/?token=FxQYhUmg6XpvtN5NsQ9PBZeP1rvKBiai
								</a>
							</td>
						</tr>
						<tr>
							<td>JSON Similarweb Internacionais</td>
							<td>
								<a href="<?php echo $url; ?>/backend/json/comparador-de-acessos-internacionais/?token=FxQYhUmg6XpvtN5NsQ9PBZeP1rvKBiai" target="_blank">
									/backend/json/comparador-de-acessos-internacionais/?token=FxQYhUmg6XpvtN5NsQ9PBZeP1rvKBiai
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="jumbotron">
				<h1>AJUSTES!</h1>
			  	<ol class="list-group">
					<li>
						Colocar Exchanges do Backend no CRON: https://dev.coinscale.io/backend/exchanges/<br>
						Atualizar o arquivo "template-json-exchnages.php" do BACKEND
					</li>
					<li>
						<strong>Revisar o MAINFRAME:</strong>
						<ul>
							<li>Separar o mainframe por duas regiões pra ficar mais leve</li>
							<li>Ajustas a roda do JSON na classe de trading</li>
							<li>Verificar questão das porcentagens</li>
							<li>Integrar ao CRON e testar</li>
						</ul>
					</li>
					<li>Para fazer a calculadora, identificar no backende pegar as 5 melhores exchanges e usar como média.</li>
					<li><strong>Terminar SIMILARWEB - VER SE CRON RODOU DIA 7 - Ver se gravou quando virar o mês</strong></li>
					<li><strong>COLOCAR HTACCESS PRA TRAVAR!!!!</strong></li>
					<li><strong>TRAVAR DOCUMENTAÇÃO COM LOGIN E SENHA!!!!</strong></li>
					<li><strong>*** LIMPAR O BACKEND ***</strong></li>
				</ol>
			</div>			
			
			<div class="panel panel-info">
				<div class="panel-heading">Endpoints</div>
				<table class="table table-striped">
	  				<thead class="thead-dark">
						<tr>
							<th scope="col">Tipo</th>
							<th scope="col">Endpoint</th>
							<th scope="col" colspan="2">Segmentação</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><strong>Arbitragem</strong></td>
							<td><a href="<?php echo $url.'/api'; ?>/trading" target="_blank">/trading</a></td>
							<td colspan="2">--</td>
						</tr>
						<tr>
							<td>Arbitragem por região</td>
							<td><a href="<?php echo $url.'/api'; ?>/trading/location/{location}" target="_blank">/trading/location/{location}</a></td>
							<td colspan="2">
								<ul class="list-group">
									<li>br</li>
									<li>global</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td>Arbitragem por cripto</td>
							<td><a href="<?php echo $url.'/api'; ?>/trading/location/{location}/{CRIPTO}" target="_blank">/trading/cryptocurrency/{CRIPTO}</a></td>
							<td colspan="2">
								<ul class="list-group">
									<li>BTC</li>
									<li>LTC</li>
									<li>ETH</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td>Arbitragem por região e cripto</td>
							<td><a href="<?php echo $url.'/api'; ?>/trading/location/{location}/{CRIPTO}" target="_blank">/trading/location/{location}/cryptocurrency/{CRIPTO}</a></td>
							<td>
								<ul class="list-group">
									<li>br</li>
									<li>global</li>
								</ul>
							</td>
							<td>
								<ul class="list-group">
									<li>BTC</li>
									<li>LTC</li>
									<li>ETH</li>
								</ul>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="panel panel-info">
				<div class="panel-heading">Taxas</div>
				<table class="table table-striped">
					<thead class="thead-dark">
						<tr>
							<th scope="col">Tipo</th>
							<th scope="col">Endpoint</th>
							<th scope="col" colspan="2">Segmentação</th>
						</tr>
					</thead>
	  				<tbody>
						<tr>
							<td>Taxas por região</td>
							<td><a href="<?php echo $url.'/api'; ?>/fee/location/{location}" target="_blank">/fee/location/{location}</a></td>
							<td colspan="2">
								<ul class="list-group">
									<li>br</li>
									<li>global</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td>Taxas por cripto</td>
							<td><a href="<?php echo $url.'/api'; ?>/fee/cryptocurrency/{CRIPTO}" target="_blank">/fee/cryptocurrency/{CRIPTO}</a></td>
							<td colspan="2">
								<ul class="list-group">
									<li>BTC</li>
									<li>LTC</li>
									<li>ETH</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td>Taxas por região e cripto</td>
							<td><a href="<?php echo $url.'/api'; ?>/fee/location/{location}/cryptocurrency/{CRIPTO}" target="_blank">/fee/location/{location}/cryptocurrency/{CRIPTO}</a></td>
							<td>
								<ul class="list-group">
									<li>br</li>
									<li>global</li>
								</ul>
							</td>
							<td>
								<ul class="list-group">
									<li>BTC</li>
									<li>LTC</li>
									<li>ETH</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td>Taxas por exchange</td>
							<td><a href="<?php echo $url.'/api'; ?>/fee/exchange/{nome da exchange}" target="_blank">/fee/exchange/{nome da exchange}</a></td>
							<td colspan="2">
								<a href="#" target="_blank">Lista de exchanges adicionadas com taxas</a></li>
							</td>
						</tr>
						<tr>
							<td>Critérios</td>
							<td><a href="<?php echo $url.'/api'; ?>/fee/criteria" target="_blank">/fee/criteria</a></td>
							<td colspan="2">
								<p>Critérios de notas para as taxas<p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="panel panel-info">
				<div class="panel-heading">Exchanges</div>
				<table class="table table-striped">
					<thead class="thead-dark">
						<tr>
							<th scope="col">Tipo</th>
							<th scope="col">Endpoint</th>
							<th scope="col" colspan="2">Segmentação</th>
						</tr>
					</thead>
	  				<tbody>
						<tr>
							<td>Dados de uma exchange específica</td>
							<td><a href="<?php echo $url.'/api'; ?>/exchanges/location/{location}" target="_blank">/exchanges/{nome da exchange}</a></td>
							<td colspan="2">
								<ul class="list-group">
									<?php foreach ($exchangesAdicionadas as $exchg): ?>
										<li><?php echo $exchg; ?></li>
									<?php endforeach; ?>
								</ul>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="panel panel-info">
				<div class="panel-heading">Acessos - SimilarWeb</div>
				<table class="table table-striped">
	  				<thead class="thead-dark">
						<tr>
							<th scope="col">Tipo</th>
							<th scope="col">Endpoint</th>
							<th scope="col" colspan="2">Segmentação</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Acessos</td>
							<td><a href="<?php echo $url.'/api'; ?>/access/{location}" target="_blank">/access/{location}</a></td>
							<td colspan="2">
								<ul class="list-group">
									<li>br</li>
									<li>global</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td>Critérios</td>
							<td><a href="<?php echo $url.'/api'; ?>/access/criteria" target="_blank">/access/criteria</a></td>
							<td colspan="2">
								<p>Critérios de notas para os acessos<p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			
		</div>
	</div>
</div>
</body>
</html>