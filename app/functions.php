<?php

/* ----------------------------------------------------------------------------------

FUNÇÕES DO PROJETO

---------------------------------------------------------------------------------- */

/* ROTAS */

function urlRoutes()
{

	$pathName = explode('/', $_SERVER['REQUEST_URI']);

	//print_r($pathName);

	if( isset($pathName[4]) && $pathName[4] )
	{
		$pageFile = 'pages/'.$pathName[2].'-'.$pathName[3].'-'.$pathName[4].'.php';
		$pageName = $pathName[4];
	}
	else if( isset($pathName[3]) && $pathName[3] )
	{
		$pageFile = 'pages/'.$pathName[2].'-'.$pathName[3].'.php';
		$pageName = $pathName[3];
	}
	else if( isset($pathName[2]) && $pathName[2] )
	{
		$pageFile = 'pages/'.$pathName[2].'.php';
		$pageName = $pathName[2];
	}
	else
	{
		$pageFile = 'pages/login.php';
		$pageName = 'login';
	}

	return array(
		'page-file' => $pageFile,
		'page-name'	=> $pageName
	);
}