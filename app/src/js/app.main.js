
/* IMPORTAÇÕES */

import '/app/src/js/plugins/jquery-3.4.1.min.js';
import '/app/src/js/modules/app.firebase.js';

import Login from '/app/src/js/modules/app.login.js';
import Trader from '/app/src/js/modules/app.trading.js';

/* INSTANCIA CLASSES */

const register = new Login;
const trade = new Trader;

/* RUN EVENTOS HABILITANDO JQUERY */

$(function()
{
    //register.loginControll();
   
    trade.tradingControll();

    // if( $('main#pageLogin') )
    // {
    // }

    $(window).resize(function()
    {
    });

    window.onload = function()
    {    
    }();
});