// function loginTest()
// {
//     return 'teste';
// }

export default class Trader
{
	renderTableLocation(location,crypto)
    {
		const coin = crypto;

		function returnFee(feeArr)
		{
			if(feeArr.length > 0)
			{
				for(var i = 0; i < feeArr.length; i++)
				{
					return feeArr[i][coin];
				}
			}
			else
			{
				return '--';
			}
		}

		function returnAccess(accessRange)
		{
			if(accessRange)
			{
				return accessRange;
			}
			else
			{
				return '--';
			}
		}

		function returnValues(type,arrValues)
		{
			switch(type)
			{
				case "last":
					return arrValues[coin].values.last;
				break;
				case "buy":
					return arrValues[coin].book.br_buy;
				break;
				case "sell":
					return arrValues[coin].book.br_sell;
				break;
				case "volume":
					return arrValues[coin].values.volume;
				break;
			}
		}

		function sortResult(a,b)
		{
			var val1 = a[1][coin]['values']['volume'];
			var val2 = b[1][coin]['values']['volume']; 
			
			return ((val1 < val2) ? 1 : ((val1 > val2) ? -1 : 0));
		}

		function renderTable(location,crypto)
		{
			//var path = 'https://coinscale.io/api/trading/location/'+location;
			var path = window.location.origin+'/api/trading/location/'+location;

			var getTradeResults = $.ajax(
			{
				type: "get",
				url: path,
				dataType: "json"
			});

			getTradeResults.done(function(result,statusText,xhr)
			{
				var html;

				if(xhr.status == 200)
				{
					var resultExchanges = Object.keys(result).map(function(key)
					{
						return [key, result[key]];
					});
					
					resultExchanges.sort(sortResult);

					for(var i = 0; i < resultExchanges.length; i++)
					{
						var value = resultExchanges[i][1];

						if(returnValues('volume',value) >= 1)
						{
							html += `
								<tr>
									<td width="40"><img src="${value.info.thumbnail}" style="width:100%; height=auto;"></td>
									<td>${value.info.title}</td>
									<td>${ returnValues('last',value) }</td>
									<td>${ returnValues('sell',value) }</td>
									<td>${ returnValues('volume',value) }</td>
									<td>${ returnFee( value.info.range.fee ) }</td>
									<td>${ returnAccess( value.info.range.access.range) }</td>
								</tr>
							`;
						}
					}

					$('table#tradeTable tbody').html(html);
				}
			});
		}

		renderTable(location);

		var start = 30000;

		setInterval(function()
		{
			if(start > 1000)
			{
				$('.countdown strong').text( (parseInt(start) - 1000) / 1000 );
				start -= 1000;
			}
			else
			{
				$('.countdown strong').text( 30 );
				start = 30000;
				renderTable(location);
			}
		}, 1000);
    }

    tradingControll()
    {
       const locationTable = this.renderTableLocation('br','BTC');
    }

    
}