<?php

require_once 'functions.php';

$GLOBALS["app"] = array(
	'path' 	=> 'https://'.$_SERVER['HTTP_HOST'].'/app',
);

require_once 'partials/header.php';

if(!@include( urlRoutes()['page-file'] ) )
{
	include 'pages/404.php';
}

require_once 'partials/footer.php';