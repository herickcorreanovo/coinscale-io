const {
	src,
	dest,
	watch,
	parallel
} = require('gulp');

//const uglify 		= require('gulp-uglify');
const uglify 		= require('gulp-uglify-es').default;
const concat 		= require('gulp-concat');
const stripDebug 	= require('gulp-strip-debug');
const lessImport 	= require('gulp-less-import');
const less 			= require('gulp-less');
const lessMap 		= require('gulp-less-sourcemap');
const minifyCSS 	= require('gulp-csso');
const rename 		= require('gulp-rename');
const imagemin 		= require('gulp-imagemin');
const webp 			= require('gulp-webp');
const sourcemaps 	= require('gulp-sourcemaps');

function lesscss()
{
	return src('src/less/style.less')
		.pipe(lessImport('style.less'))
		.pipe(less('style.css'))
		.pipe(minifyCSS())
		.pipe(lessMap({
	        sourceMap: {
	            sourceMapRootpath: './css'
	        }
	    }))
		.pipe(dest('./css'))
}

function minscripts()
{
	return src('src/js/app.main.js')
		.pipe(sourcemaps.init())
		//.pipe(concat('app.main.min.js'))
		.pipe(rename("app.main.min.js"))
		.pipe(uglify())
		.pipe(sourcemaps.write('./'))
		.pipe(dest('./js'));
}

/*
function minscriptsPlugins()
{
	return src('src/js-plugins/*.js')
		.pipe(concat('plugins.js'))
		.pipe(stripDebug())
		.pipe(uglify())
		.pipe(dest('./js'));
}
*/

function optimizeImagesWebp()
{
	return src('src/images/*.{jpg,jpeg,png}')
		.pipe(webp())
		.pipe(dest('./images'));
}

function mwatch()
{
	watch('src/less/*.less', lesscss);
	watch('src/js/*.js', minscripts);
	//watch('src/js-plugins/*.js', minscriptsPlugins);
	watch('src/images/*.{jpg,jpeg,png}', optimizeImagesWebp);
}

exports.watch 	= mwatch;
exports.js 		= parallel(minscripts);
exports.css 	= parallel(lesscss);
exports.compile	= parallel(lesscss, minscripts, optimizeImagesWebp);