<?php header('Content-type: text/html; charset=utf-8'); ?>
<!doctype html>
<html amp lang="pt-br">
<head>
	<title>coinscale.io - Dashboard for Traders</title>
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<link rel="canonical" href="<?php echo $app['path'].'/'.urlRoutes()['page-name']; ?>">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<script src="https://www.gstatic.com/firebasejs/6.6.2/firebase-app.js"></script>
	<script src="https://www.gstatic.com/firebasejs/6.6.2/firebase-auth.js"></script>
	<script src="https://www.gstatic.com/firebasejs/6.6.2/firebase-database.js"></script>
</head>
<body class="<?php echo 'current-page-'.urlRoutes()['page-name']; ?>" data-path="/app/">