<main role="main" id="pageLogin">
	<section id="login">
		<div class="container">
			<div class="row"></div>
		</div>
	</section>

	<section id="register">
		<div class="container">
			<div class="row">
				<form id="userRegister" class="formularioStyle">
					<fieldset>
						<legend>Cadastrar</legend>
						<div class="input col-xs-6">
							<label for="nome">Nome:</label>
							<input type="text" id="nome" name="nome" class="defaultInputStyle">
						</div>
						<div class="input col-xs-6">
							<label for="sobrenome">Sobrenome:</label>
							<input type="text" id="sobrenome" name="sobrenome" class="defaultInputStyle">
						</div>
						<div class="input col-xs-12">
							<label for="email">E-mail:</label>
							<input type="email" id="email" name="email" class="defaultInputStyle">
						</div>
						<div class="input col-xs-12">
							<label for="password">Senha:</label>
							<input type="password" id="password" name="password" class="defaultInputStyle">
						</div>
						<div class="input col-xs-12">
							<label for="sexo">Sexo:</label>
							<div class="select">
								<select name="sexo" id="sexo" type="select">
									<option>Masculino</option>
									<option>Feminino</option>
								</select>
							</div>
						</div>
						<div class="input col-xs-12">
							<label for="cidade">Cidade:</label>
							<input type="text" id="cidade" name="cidade" class="defaultInputStyle">
						</div>
						<div class="cpatcha col-xs-12">
							<button class="btn full azul" form="userRegister" type="submit">Cadastrar</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</section>
</main>
