<?php
class Poloniex
{	
	function getValues($coin)
	{
		$json = json_decode(file_get_contents('https://poloniex.com/public?command=returnTicker'),true);
		
		return array(
		    'buy' 		=> $json['USDC_BTC']['highestBid'],
            'high'    	=> $json['USDC_BTC']['high24hr'],
            'low'     	=> $json['USDC_BTC']['low24hr'],
            'sell'    	=> $json['USDC_BTC']['lowestAsk'],
            'last'    	=> $json['USDC_BTC']['last'],
            'volume'  	=> $json['USDC_BTC']['baseVolume'],
		);
	}

	function getBookDeep($coin)
	{
		$json = json_decode(file_get_contents('https://poloniex.com/public?command=returnOrderBook&currencyPair='.$coin.'&depth=50'),true);

		$bookQtdeBuy = 1;
		$bookPriceBuy = 0;

		$bookQtdeSell = 0;
		$bookPriceSell = 0;

		foreach ($json['bids'] as $ordens)
		{
			$price = floatval($ordens[0]);
			$qtde = floatval($ordens[1]);
			
			if($bookQtdeBuy > 0)
			{
				$bookQtdeBuy -= $qtde;
				$bookPriceBuy = $price;
			}
		}

		foreach ($json['asks'] as $ordens)
		{
			$price = floatval($ordens[0]);
			$qtde = floatval($ordens[1]);
			
			if($bookQtdeSell < 1)
			{
				$bookQtdeSell += $qtde;
				$bookPriceSell = $price;
			}
		}

		return array(
			'buy'	=> $bookPriceBuy,
			'sell' 	=> $bookPriceSell,
		);
	}

	//construtor da classe
    function returnedArray($id,$coin,$pair)
    {
    	return array(
    		'exchange'	=> 'Poloniex',
    		'symbol' 	=> strtolower($pair),
    		'folder' 	=> 'poloniex',
			'valores' 	=> $this->getValues($coin),
			'deepBook' 	=> $this->getBookDeep($coin),
			'info' 		=> parent::exchangeInfos($id),
		);
    }
}