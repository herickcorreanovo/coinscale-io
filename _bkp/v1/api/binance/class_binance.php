<?php

class Binance extends Extras
{
	function getValues($coin)
	{
		$json = json_decode(file_get_contents('https://api.binance.com/api/v1/ticker/24hr?symbol='.$coin),true);

		return array(
		    'buy' 		=> $json['bidPrice'],
            'high'    	=> $json['highPrice'],
            'low'     	=> $json['lowPrice'],
            'sell'    	=> $json['askPrice'],
            'last'    	=> $json['lastPrice'],
            'volume'  	=> $json['quoteVolume'],
		);
	}

	function getBookDeep($coin)
	{
		$json = json_decode(file_get_contents('https://api.binance.com/api/v1/depth?symbol='.$coin.'&limit=50'),true);

		$bookQtdeBuy = 1;
		$bookPriceBuy = 0;

		$bookQtdeSell = 0;
		$bookPriceSell = 0;

		foreach ($json['bids'] as $ordens)
		{
			$price = floatval($ordens[0]);
			$qtde = floatval($ordens[1]);
			
			if($bookQtdeBuy > 0)
			{
				$bookQtdeBuy -= $qtde;
				$bookPriceBuy = $price;
			}
		}

		foreach ($json['asks'] as $ordens)
		{
			$price = floatval($ordens[0]);
			$qtde = floatval($ordens[1]);
			
			if($bookQtdeSell < 1)
			{
				$bookQtdeSell += $qtde;
				$bookPriceSell = $price;
			}
		}

		return array(
			'buy'	=> $bookPriceBuy,
			'sell' 	=> $bookPriceSell,
		);
	}

	//construtor da classe
    function returnedArray($id,$coin,$pair)
    {
    	return array(
    		'exchangeinfo'	=> 'null',//parent::exchangeInfos($id),
    		'symbol' 		=> strtolower($pair),
    		'folder' 		=> 'binance',
    		'pair' 			=> strtolower($pair),
			'values' 		=> $this->getValues($coin),
			'deepBook'		=> $this->getBookDeep($coin),
		);
    }
}