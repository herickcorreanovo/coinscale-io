<?php

if( (isset($_GET['pair']) && $_GET['pair'] != '') && (isset($_GET['api_key']) && $_GET['api_key'] != '') )
{
	$GLOBALS["apiKey"] = $_GET['api_key'];
	$GLOBALS["jsonfile"] = explode( '/?' , str_replace('/api/','',$_SERVER['REQUEST_URI']) )[0].'_'.$_GET['pair'].'.json';

	include '../trigger_api.php';
}
else
{
	echo ':-( this endpoint does not exist...';
}