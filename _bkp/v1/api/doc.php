<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Documentação COINSCALE.IO</title>
	<script src="https://use.fontawesome.com/9f6cfc4141.js"></script>
</head>
<body>
	<?php $url = 'https://'.$_SERVER['HTTP_HOST'].'/api'; ?>
	<h1>Documentação coinscale.io</h1>
	<p><strong>Endpoint:</strong> <?php echo $url; ?></p>
	<hr>
	<h2>TAXAS</h2>
	<h3>Tokens</h3>
	<table border="1">
		<thead>
			<tr>
				<td><strong>Domínio</strong></td>
				<td><strong>Token</strong></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>coinscale.io</td>
				<td>FxQYhUmg6XpvtN5NsQ9PBZeP1rvKBiai</td>
			</tr>
			<tr>
				<td>comparadores.webitcoin.com.br</td>
				<td>FxQYhUmg6XpvtN5NsQ9PBZeP1rvKBiai</td>
			</tr>
		</tbody>
	</table>
	<h3>Parâmentros</h3>
	<table border="1">
		<thead>
			<tr>
				<td><strong>Parâmetro</strong></td>
				<td><strong>Valores</strong></td>
				<td><strong>Default</strong></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>&amp;api_key</td>
				<td>
					Ask for your API Key
				</td>
				<td>-</td>
			</tr>
			<tr>
				<td>&amp;regiao</td>
				<td>
					2 (Brasil), 3 (Internacional) 
				</td>
				<td>2</td>
			</tr>
		</tbody>
	</table>
	<br>
	<hr>
	<h2>TAXAS Exchanges</h2>
	<table border="1">
		<thead>
			<tr>
				<td><strong>Região</strong></td>
				<td><strong>Endpoint</strong></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Brasil</td>
				<td><a href="<?php echo $url; ?>/_br/rates">/_br/rates/?api_key={key}</a></td>
			</tr>
			<tr>
				<td>Internacionais</td>
				<td><a href="<?php echo $url; ?>/_internacional/rates">/_internacional/rates/?api_key={key}</a></td>
			</tr>
			<tr>
				<td>Global</td>
				<td><a href="<?php echo $url; ?>/_global/rates">/_global/rates/?api_key={key}</a></td>
			</tr>
		</tbody>
	</table>
	<br>
	<hr>
	<h2>Lista Completa de Exchanges</h2>
	<table border="1">
		<thead>
			<tr>
				<td><strong>Região</strong></td>
				<td><strong>Endpoint</strong></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Brasil</td>
				<td><a href="<?php echo $url; ?>/_br/exchanges">/_br/exchanges/?api_key={key}</a></td>
			</tr>
			<tr>
				<td>Internacionais</td>
				<td><a href="<?php echo $url; ?>/_internacional/exchanges">/_internacional/exchanges/?api_key={key}</a></td>
			</tr>
			<tr>
				<td>Global</td>
				<td><a href="<?php echo $url; ?>/_global/exchanges">/_global/exchanges/?api_key={key}</a></td>
			</tr>
		</tbody>
	</table>
	<br>
	<hr>
	<h2>BTC</h2>
	<h3>Nacionais</h3>
	<table border="1">
		<thead>
			<tr>
				<td><strong>Exchange</strong></td>
				<td><strong>Método</strong></td>
				<td><strong>Endereço</strong></td>
				<td><strong>Real</strong></td>
				<td><strong>Dolar</strong></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>3xBit</td>
				<td valign="center">GET</td>
				<td><a href="<?php echo $url; ?>/3xbit/?pair=btcbrl">/3xbit/?pair={btcbrl}&api_key={key}</a></td>
				<td valign="center"><i class="fa fa-check" aria-hidden="true"></i></td>
				<td valign="center"><i class="fa fa-check" aria-hidden="true"></i></td>
			</tr>
			<tr>
				<td>TemBTC</td>
				<td valign="center">GET</td>
				<td><a href="<?php echo $url; ?>/tembtc/?pair=btcbrl">/tembtc/?pair={btcbrl}&api_key={key}</a></td>
				<td valign="center"><i class="fa fa-check" aria-hidden="true"></i></td>
				<td valign="center"><i class="fa fa-times" aria-hidden="true"></i></td>
			</tr>
			<tr>
				<td>Negociecoins</td>
				<td valign="center">GET</td>
				<td><a href="<?php echo $url; ?>/negociecoins/?pair=btcbrl">/negociecoins/?pair={btcbrl}&api_key={key}</a></td>
				<td valign="center"><i class="fa fa-check" aria-hidden="true"></i></td>
				<td valign="center"><i class="fa fa-times" aria-hidden="true"></i></td>
			</tr>
			<tr>
				<td>MercadoBitcoin</td>
				<td valign="center">GET</td>
				<td><a href="<?php echo $url; ?>/mercadobitcoin/?pair=btcbrl">/mercadobitcoin/?pair={btcbrl}&api_key={key}</a></td>
				<td valign="center"><i class="fa fa-check" aria-hidden="true"></i></td>
				<td valign="center"><i class="fa fa-times" aria-hidden="true"></i></td>
			</tr>
		</tbody>
	</table>
	<h3>Internacionais</h3>
	<table border="1">
		<thead>
			<tr>
				<td><strong>Exchange</strong></td>
				<td><strong>Método</strong></td>
				<td><strong>Endereço</strong></td>
				<td><strong>Real</strong></td>
				<td><strong>Dolar</strong></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Binance</td>
				<td valign="center">GET</td>
				<td><a href="<?php echo $url; ?>/binance/?pair=btcusdt">/binance/?pair={pair}&api_key={key}</a></td>
				<td valign="center"><i class="fa fa-times" aria-hidden="true"></i></td>
				<td valign="center"><i class="fa fa-check" aria-hidden="true"></i></td>
			</tr>
			<tr>
				<td>Poloniex</td>
				<td valign="center">GET</td>
				<td><a href="<?php echo $url; ?>/poloniex/?pair=btcusdt">/poloniex/?pair={pair}&api_key={key}</a></td>
				<td valign="center"><i class="fa fa-times" aria-hidden="true"></i></td>
				<td valign="center"><i class="fa fa-check" aria-hidden="true"></i></td>
			</tr>
		</tbody>
	</table>
</body>
</html>