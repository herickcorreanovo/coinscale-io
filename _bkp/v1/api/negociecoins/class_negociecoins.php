<?php
class Negociecoins
{
	function getValues($currency)
	{
		$json = json_decode(file_get_contents('https://broker.negociecoins.com.br/api/v3/'.$currency.'/ticker'),true);

		return array(
		    'buy' 		=> $json['buy'],
            'sell'    	=> $json['sell'],
            'last'    	=> $json['last'],
            'high'    	=> $json['high'],
            'low'     	=> $json['low'],
            'volume'  	=> $json['vol'],
		);
	}

	function getBookDeep($currency)
	{
		//sleep(5);

		$json = json_decode(file_get_contents('https://broker.negociecoins.com.br/api/v3/'.$currency.'/orderbook'),true);

		$bookQtdeBuy = 1;
		$bookPriceBuy = 0;

		$bookQtdeSell = 0;
		$bookPriceSell = 0;

		foreach ($json['bid'] as $ordens)
		{
			$price = floatval($ordens['price']);
			$qtde = floatval($ordens['quantity']);
			
			if($bookQtdeBuy > 0)
			{
				$bookQtdeBuy -= $qtde;
				$bookPriceBuy = $price;
			}
		}

		foreach ($json['ask'] as $ordens)
		{
			$price = floatval($ordens['price']);
			$qtde = floatval($ordens['quantity']);
			
			if($bookQtdeSell < 1)
			{
				$bookQtdeSell += $qtde;
				$bookPriceSell = $price;
			}
		}

		return array(
			'buy'	=> $bookPriceBuy,
			'sell' 	=> $bookPriceSell,
		);
	}

	//construtor da classe
    function returnedArray($coin,$brl,$par)
    {
    	return array(
    		'exchange'	=> 'Negociecoins',
    		'symbol' 	=> strtolower($coin),
    		'folder' 	=> 'negociecoins',
    		'pair'	 	=> $par,
			'values'	=> $this->getValues($brl),
			'deepBook' 	=> $this->getBookDeep($brl),
		);
    }
}