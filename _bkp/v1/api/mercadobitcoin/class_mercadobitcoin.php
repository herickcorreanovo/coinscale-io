<?php
class MercadoBitcoin
{
	function getValues($coin)
	{
		$json = json_decode(file_get_contents('https://www.mercadobitcoin.net/api/'.$coin.'/ticker/'),true);

		return array(
		    'buy' 		=> $json['ticker']['buy'],
            'sell'    	=> $json['ticker']['sell'],
            'last'    	=> $json['ticker']['last'],
            'high'    	=> $json['ticker']['high'],
            'low'     	=> $json['ticker']['low'],
            'volume'  	=> $json['ticker']['vol'],
		);
	}

	function getBookDeep($coin)
	{
		//sleep(5);

		$json = json_decode(file_get_contents('https://www.mercadobitcoin.net/api/'.$coin.'/orderbook/'),true);

		$bookQtdeBuy = 1;
		$bookPriceBuy = 0;

		$bookQtdeSell = 0;
		$bookPriceSell = 0;

		foreach ($json['bids'] as $ordens)
		{
			$price = floatval($ordens[0]);
			$qtde = floatval($ordens[1]);
			
			if($bookQtdeBuy > 0)
			{
				$bookQtdeBuy -= $qtde;
				$bookPriceBuy = $price;
			}
		}

		foreach ($json['asks'] as $ordens)
		{
			$price = floatval($ordens[0]);
			$qtde = floatval($ordens[1]);
			
			if($bookQtdeSell < 1)
			{
				$bookQtdeSell += $qtde;
				$bookPriceSell = $price;
			}
		}

		return array(
			'buy'	=> $bookPriceBuy,
			'sell' 	=> $bookPriceSell,
		);
	}

	//construtor da classe
    function returnedArray($coin,$par)
    {
    	return array(
    		'exchange'	=> 'MercadoBitcoin',
    		'symbol' 	=> strtolower($coin),
    		'folder' 	=> 'mercadobitcoin',
    		'pair' 		=> $par,
			'values'	=> $this->getValues($coin),
			'deepBook' 	=> $this->getBookDeep($coin),
		);
    }
}