<h1>Roda o Crawler</h1>

<?php

require_once 'extras.php';

/* REQUIRE CLASSES NACIONAIS */

require_once '3xbit/class_3xbit.php';
require_once 'tembtc/class_tembtc.php';
require_once 'negociecoins/class_negociecoins.php';
require_once 'mercadobitcoin/class_mercadobitcoin.php';


/* REQUIRE CLASSES INTERNACIONAIS */

require_once 'binance/class_binance.php';
require_once 'poloniex/class_poloniex.php';


/* INSTACIA EXCHANGES NACIONAIS */

$result_3xbit 			= new ThreexBit();
$result_tembtc 			= new TemBTC();
$result_negociecoins	= new Negociecoins();
$result_mercadobitcoin 	= new MercadoBitcoin();


/* INSTACIA EXCHANGES INTERNACIONAIS */

$result_binance 	= new Binance();
$result_poloniex 	= new Poloniex();




/* CONSTRÓI LOOP NACIONAIS  */

$btc_brl = array(
	'3xbit' 			=> $result_3xbit->returnedArray('btc','brl','btcbrl'),
	'tembtc' 			=> $result_tembtc->returnedArray('btc','btcbrl','btcbrl'),
	'negociecoins' 		=> $result_negociecoins->returnedArray('btc','btcbrl','btcbrl'),
	'mercadobitcoin' 	=> $result_mercadobitcoin->returnedArray('btc','btcbrl'),
);

foreach($btc_brl as $exchange)
{
	$json 		= json_encode($exchange);
	$folder 	= $exchange['folder'];
	$filename 	= $exchange['folder'].'_'.$exchange['pair'].'.json';

	$file = fopen($folder.'/'.$filename,'w+');
	fwrite($file, $json);
	fclose($file);
}

/* CONSTRÓI LOOP INTERNACIONAIS */

$btc_usdt = array(
	'binance' 	=> $result_binance->returnedArray(74,'BTCUSDT','btcusdt'),
	//'poloniex' 	=> $result_poloniex->returnedArray(77,'USDT_BTC','btcusdt'),
);

foreach($btc_usdt as $exchange)
{
	$json 		= json_encode($exchange);
	$folder 	= $exchange['folder'];
	$filename 	= $exchange['folder'].'_'.$exchange['symbol'].'.json';

	$file = fopen($folder.'/'.$filename,'w+');
	fwrite($file, $json);
	fclose($file);
}

/* RENDER RESULT */

echo '<pre>';
print_r( $btc_brl );
//print_r( $btc_usdt );
echo '</pre>';

/* ESCREVE FILE BR */

$fileBR = fopen('_br/exchanges/exchanges.json','w+');

fwrite( $fileBR , json_encode($btc_brl) );
fclose( $fileBR );

/* ESCREVE FILE NACIONAIS 

$fileGlobal = fopen('_internacional/exchanges/internacional_exchanges.json','w+');

fwrite( $fileGlobal , json_encode($btc_usdt) );
fclose( $fileGlobal );
*/