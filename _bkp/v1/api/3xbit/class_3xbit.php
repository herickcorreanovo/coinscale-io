<?php
class ThreexBit
{
	function getValues($currency)
	{
		$json = json_decode(file_get_contents('https://api.exchange.3xbit.com.br/ticker/'.$currency),true);

		return array(
		    'buy' 		=> $json['CREDIT_BTC']['bid'],
            'sell'    	=> $json['CREDIT_BTC']['ask'],
            'last'    	=> $json['CREDIT_BTC']['last'],
            'high'    	=> $json['CREDIT_BTC']['max'],
            'low'     	=> $json['CREDIT_BTC']['min'],
            'volume'  	=> $json['CREDIT_BTC']['volume_market'],
		);
	}

	function getBookDeep($coin,$currency)
	{
		$json = json_decode(file_get_contents('https://api.exchange.3xbit.com.br/v1/orderbook/credit/'.$coin.'/?currency_rate='.$currency),true);

		$bookQtdeBuy = 1;
		$bookPriceBuy = 0;

		$bookQtdeSell = 0;
		$bookPriceSell = 0;

		foreach ($json['buy_orders'] as $ordens)
		{
			$price = floatval($ordens['unit_price']);
			$qtde = floatval($ordens['quantity']);
			
			if($bookQtdeBuy > 0)
			{
				$bookQtdeBuy -= $qtde;
				$bookPriceBuy = $price;
			}
		}

		foreach ($json['sell_orders'] as $ordens)
		{
			$price = floatval($ordens['unit_price']);
			$qtde = floatval($ordens['quantity']);
			
			if($bookQtdeSell < 1)
			{
				$bookQtdeSell += $qtde;
				$bookPriceSell = $price;
			}
		}

		return array(
			'buy'	=> $bookPriceBuy,
			'sell' 	=> $bookPriceSell,
		);
	}

	//construtor da classe
    function returnedArray($coin,$brl,$par)
    {
    	return array(
    		'exchange'	=> '3xBit',
    		'symbol' 	=> strtolower($coin),
    		'folder' 	=> '3xbit',
    		'pair'	 	=> $par,
			'values'	=> $this->getValues($brl),
			'deepBook' 	=> $this->getBookDeep($coin,$brl),
		);
    }
}