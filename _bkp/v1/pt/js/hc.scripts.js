$(function()
{
	var getExchangesNacionais = $.ajax(
	{
        type: "get",
        url: window.location.origin+'/api/_br/br_exchanges.json',
        dataType: "json"
    });

    getExchangesNacionais.done(function(result)
    {
    	var html = '';

    	var array = $.map(result, function(value, index)
    	{
		    var exchange 	= [value][0],
		    	nome 		= exchange.exchange,
		    	preco 		= parseFloat(exchange.valores_br.last),
		    	compra 		= parseFloat(exchange.deepBook_br.buy, 2),
		    	venda 		= parseFloat(exchange.deepBook_br.sell, 2),
		    	high 		= parseFloat(exchange.valores_br.high, 2),
		    	low 		= parseFloat(exchange.valores_br.low, 2),
		    	volume 		= exchange.valores_br.volume;
		   	
		   	html += `
		    	<tr>
                    <td><span>icon +</span> `+nome+`</td>
                    <td>R$ `+preco.toFixed(2)+`</td>
                    <td data-hide="phone">R$ `+compra.toFixed(2)+`</td>
                    <td data-hide="phone">R$ `+venda.toFixed(2)+`</td>
                    <td data-hide="phone,tablet">R$ `+high.toFixed(2)+`</td>
                    <td data-hide="phone,tablet">R$ `+low.toFixed(2)+`</td>
                    <td>R$ `+volume+`</td>
                </tr>
		    `;
		});

		$('table#tabelaArbitragem tbody').html(html);
    });

    var getExchangesInternacionais = $.ajax(
	{
        type: "get",
        url: window.location.origin+'/api/_global/global_exchanges.json',
        dataType: "json"
    });

    getExchangesInternacionais.done(function(result)
    {
    	var html = '';

    	var array = $.map(result, function(value, index)
    	{
		    var exchange 	= [value][0],
		    	nome 		= exchange.exchange,
		    	preco 		= parseFloat(exchange.valores.last),
		    	compra 		= parseFloat(exchange.deepBook.buy, 2),
		    	venda 		= parseFloat(exchange.deepBook.sell, 2),
		    	high 		= parseFloat(exchange.valores.high, 2),
		    	low 		= parseFloat(exchange.valores.low, 2),
		    	volume 		= exchange.valores.volume;
		   	
		   	html += `
		    	<tr>
                    <td><span>icon +</span> `+nome+`</td>
                    <td>R$ `+preco.toFixed(2)+`</td>
                    <td data-hide="phone">R$ `+compra.toFixed(2)+`</td>
                    <td data-hide="phone">R$ `+venda.toFixed(2)+`</td>
                    <td data-hide="phone,tablet">R$ `+high.toFixed(2)+`</td>
                    <td data-hide="phone,tablet">R$ `+low.toFixed(2)+`</td>
                    <td>R$ `+volume+`</td>
                </tr>
		    `;
		});

		$('table#tabelaArbitragemInternacional tbody').html(html);
    });
});