<?php include 'header.php'; ?>
    <div id="page-wrapper" class="gray-bg sidebar-content">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="#">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>

            </nav>
        </div>

        <div class="sidebar-panel">
            
            <div class="row">
                <div class="ibox ">
                    <div class="ibox-content ibox-heading">
                        <h3><i class="fa fa-bitcoin"></i> Webitcoin</h3>
                    </div>
                    <div class="ibox-content">
                        <div class="feed-activity-list">

                            <div class="feed-element">
                                <div>
                                    <div>
                                        <a href="#" target="_blank">
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                        </a>
                                    </div>
                                    <strong>Webitcoin</strong>
                                    <small class="text-muted">Today 5:60 pm</small>
                                </div>
                            </div>

                            <div class="feed-element">
                                <div>
                                    <div>
                                        <a href="#" target="_blank">
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                        </a>
                                    </div>
                                    <strong>Webitcoin</strong>
                                    <small class="text-muted">Today 2:23 pm</small>
                                </div>
                            </div>

                            <div class="feed-element">
                                <div>
                                    <div>
                                        <a href="#" target="_blank">
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                        </a>
                                    </div>
                                    <strong>Webitcoin</strong>
                                    <small class="text-muted">Today 1:00 pm</small>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="m-t-md">
                <h4>Brasil</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.
                </p>
                <div class="row m-t-sm">
                    <div class="col-md-12">
                        <span class="bar" style="display: none;">5,3,9,6,5,9,7,3,5,2</span><svg class="peity" height="16" width="32"><rect fill="#1ab394" x="0" y="7.111111111111111" width="2.3" height="8.88888888888889"></rect><rect fill="#d7d7d7" x="3.3" y="10.666666666666668" width="2.3" height="5.333333333333333"></rect><rect fill="#1ab394" x="6.6" y="0" width="2.3" height="16"></rect><rect fill="#d7d7d7" x="9.899999999999999" y="5.333333333333334" width="2.3" height="10.666666666666666"></rect><rect fill="#1ab394" x="13.2" y="7.111111111111111" width="2.3" height="8.88888888888889"></rect><rect fill="#d7d7d7" x="16.5" y="0" width="2.3" height="16"></rect><rect fill="#1ab394" x="19.799999999999997" y="3.555555555555557" width="2.3" height="12.444444444444443"></rect><rect fill="#d7d7d7" x="23.099999999999998" y="10.666666666666668" width="2.3" height="5.333333333333333"></rect><rect fill="#1ab394" x="26.4" y="7.111111111111111" width="2.3" height="8.88888888888889"></rect><rect fill="#d7d7d7" x="29.7" y="12.444444444444445" width="2.3" height="3.5555555555555554"></rect></svg>
                        <h5><strong>36</strong> Exchanges</h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row m-b-lg m-t-lg">
                <div class="col-md-9">
                    <div class="profile-image">
                        <img src="img/a4.jpg" class="rounded-circle circle-border m-b-md" alt="profile">
                    </div>
                    <div class="profile-info">
                        <div class="">
                            <div>
                                <h2 class="no-margins">Herick Correa</h2>
                                <h4>Fundador da coinscale.io</h4>
                                <small>
                                    There are many variations of passages of Lorem Ipsum available, but the majority
                                    have suffered alteration in some form Ipsum available.
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 pull-right">
                    <!-- TradingView Widget BEGIN -->
                        <div class="tradingview-widget-container">
                            <div class="tradingview-widget-container__widget"></div>
                            <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-single-quote.js" async>
                                {
                                "symbol": "MERCADO:BTCBRL",
                                "width": "100%",
                                "colorTheme": "light",
                                "isTransparent": true,
                                "locale": "br"
                                }
                            </script>
                        </div>
                    <!-- TradingView Widget END -->                  
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">

                        <div class="ibox-title ui-sortable-handle">
                            <h5>Tabela de Arbitragem Nacionais</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="tabelaArbitragem" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Exchnage</th>
                                            <th valign="center">Preço</th>
                                            <th valign="center" data-hide="phone">Compra</th>
                                            <th valign="center" data-hide="phone">Venda</th>
                                            <th valign="center" data-hide="phone,tablet">Máx.</th>
                                            <th valign="center" data-hide="phone,tablet">Min.</th>
                                            <th valign="center">Volume</th>
                                        </tr>
                                    </thead>

                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">

                        <div class="ibox-title ui-sortable-handle">
                            <h5>Tabela de Arbitragem internacionais</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table id="tabelaArbitragemInternacional" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Exchnage</th>
                                            <th valign="center">Preço</th>
                                            <th valign="center" data-hide="phone">Compra</th>
                                            <th valign="center" data-hide="phone">Venda</th>
                                            <th valign="center" data-hide="phone,tablet">Máx.</th>
                                            <th valign="center" data-hide="phone,tablet">Min.</th>
                                            <th valign="center">Volume</th>
                                        </tr>
                                    </thead>

                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
            <?php /*
            <div class="row" id="sortable-view">
                <div class="col-lg-12 ui-sortable">
                    <div class="ibox">
                         <div class="ibox-title ui-sortable-handle">
                            <h5>TradingView</h5>
                        </div>
                        <div class="ibox-content">
                            <!-- TradingView Widget BEGIN -->
                            <div class="tradingview-widget-container">
                                <div id="tradingview_40c1e" style="height:300px; position:relative"></div>
                                <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                                <script type="text/javascript">
                                    new TradingView.widget(
                                    {
                                        "autosize": true,
                                        "symbol": "MERCADO:BTCBRL",
                                        "interval": "D",
                                        "timezone": "Etc/UTC",
                                        "theme": "Light",
                                        "style": "1",
                                        "locale": "en",
                                        "toolbar_bg": "#f1f3f6",
                                        "enable_publishing": false,
                                        "allow_symbol_change": true,
                                        "container_id": "tradingview_40c1e"
                                    });
                                </script>
                            </div>
                            <!-- TradingView Widget END -->
                        </div>
                    </div>
                </div>
            </div>
            */ ?>

        </div>
        
        <?php include 'copyright.php'; ?>
    </div>
<?php include 'footer.php'; ?>