<?php

// GERA UMA LIBRARY COM DADOS DAS EXCHANGES

require_once 'functions.php';
require_once 'classes/class_values_exchanges_nacionais.php';
require_once 'classes/class_values_exchanges_internacionais.php';

class Exchanges extends Functions
{
	function returned_BR_Exchanges()
    {
    	$valueNacionais = new ValuesExchangesNacionais();

		return array(
			'mercado-bitcoin'=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(65,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->mercadobitcoin('prices','btc'),
					'book'			=> $valueNacionais->mercadobitcoin('book','btc'),
				),
			),
			'bitcoin-trade'	=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(53,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->bitcointrade('prices','BRLBTC'),
					'book'			=> $valueNacionais->bitcointrade('book','BRLBTC'),
				),
			),
			'bitcambio'		=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(51,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->bitcambio('prices','BTC'),
					'book'			=> $valueNacionais->bitcambio('book','BTC'),
				),
			),
			'bitpreco'		=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(210,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->bitpreco('prices','btc-brl'),
					'book'			=> $valueNacionais->bitpreco('book','btc-brl'),
				),
			),
			'bitblue'		=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(58,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->bitblue('prices',''),
					'book'			=> $valueNacionais->bitblue('book',''),
				),
			),
			'bitcointoyou'	=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(52,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->bitcointoyou('prices','ticker.aspx'),
					'book'			=> $valueNacionais->bitcointoyou('book','orderbook.aspx'),
				),
			),
			'novadax'		=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(527,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->novadax('prices','BTC_BRL'),
					'book'			=> $valueNacionais->novadax('book','BTC_BRL'),
				),
			),
			'brasiliex'		=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(57,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->brasiliex('prices','btc_brl'),
					'book'			=> $valueNacionais->brasiliex('book','btc_brl'),
				),
			),			
			'bitnuvem'		=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(54,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->bitnuvem('prices','BTC'),
					'book'			=> $valueNacionais->bitnuvem('book','BTC'),
				),
			),
			'pagcripto'		=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(201,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->pagcripto('prices','BTC'),
					'book'			=> $valueNacionais->pagcripto('book','BTC'),
				),
			),
			'omnitrade'		=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(203,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->omnitrade('prices','btcbrl'),
					'book'			=> $valueNacionais->omnitrade('book','btcbrl'),
				),
			),
			'bitrecife'		=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(344,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->bitrecife('prices','BTC_BRL'),
					'book'			=> $valueNacionais->bitrecife('book','BTC_BRL'),
				),
			),
			'profitfy'		=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(70,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->profitfy('prices','BTC'),
					'book'			=> $valueNacionais->profitfy('book','BRL/BTC'),
				),
			),

			/*
			'modiax'		=> array(
				'location'		=> 'br',
				//'info'			=> parent::exchangeInfos(197,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->modiax('btcbrl'),
					'book'			=> $valueNacionais->modiax('btcbrl'),
				),
				'calc' 			=> false
			),
			'walltime'		=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(72,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->walltime(),
					//'book'		=> $valueNacionais->walltime(),
				),
				'calc' 			=> false
			),					
			'foxbit'			=> array(
				//'location'		=> 'br',
				//'info'			=> parent::exchangeInfos(64,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->foxbitTicker('btc-brl'),
					//'book'		=> $valueNacionais->foxbitTicker('btc-brl'),
				),
				'calc' 			=> true
			),
			'bitblue'		=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(58,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->bitblue(''),
					'book'			=> $valueNacionais->bitblue(''),
				),
				'calc' 			=> false
			),
			*/

			/*
			'negociecoins'	=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(67),
				'BTC'			=> array(
					'values'		=> $valueNacionais->negociecoins('btcbrl'),
					'book'			=> $valueNacionais->negociecoins('btcbrl'),
				),
				'calc' 			=> false
			),
			'tembtc'		=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(71),
				'BTC'			=> array(
					'values'		=> $valueNacionais->tembtc('btcbrl'),
					'book'			=> $valueNacionais->tembtc('btcbrl'),
				),
				'calc' 			=> false
			),
			'3xbit'			=> array(
				'location'		=> 'br',
				'info'			=> parent::exchangeInfos(50,'nacionais'),
				'BTC'			=> array(
					'values'		=> $valueNacionais->threexbit('brl'),
					'book'			=> $valueNacionais->threexbit('btc'),
				),
				'calc' 			=> true
			),
			*/
	    );
	}

	function returned_Global_Exchanges()
    {
    	$valueInternacionais = new ValuesExchangesInternacionais();

		return array(
			'binance'		=> array(
				'location'		=> 'global',
				'info'			=> parent::exchangeInfos(74,'internacionais'),
				'BTC'			=> array(
					'values'		=> $valueInternacionais->returnValues('prices','binance','BTC/USDT'),
					'book'			=> $valueInternacionais->returnValues('book','binance','BTC/USDT'),
				)
			),
			'bitmex'		=> array(
				'location'		=> 'global',
				'info'			=> parent::exchangeInfos(73,'internacionais'),
				'BTC'			=> array(
					'values'		=> $valueInternacionais->returnValues('prices','bitmex','BTC/USD'),
					'book'			=> $valueInternacionais->returnValues('book','bitmex','BTC/USD'),
				)
			),
			'kraken'		=> array(
				'location'		=> 'global',
				'info'			=> parent::exchangeInfos(82,'internacionais'),
				'BTC'			=> array(
					'values'		=> $valueInternacionais->returnValues('prices','kraken','BTC/USD'),
					'book'			=> $valueInternacionais->returnValues('book','kraken','BTC/USD'),
				)
			),
			'bitfinex'		=> array(
				'location'		=> 'global',
				'info'			=> parent::exchangeInfos(74,'internacionais'),
				'BTC'			=> array(
					'values'		=> $valueInternacionais->returnValues('prices','bitfinex','BTC/USD'),
					'book'			=> $valueInternacionais->returnValues('book','bitfinex','BTC/USD'),
				)
			),
			'coinbase-pro'		=> array(
				'location'		=> 'global',
				'info'			=> parent::exchangeInfos(78,'internacionais'),
				'BTC'			=> array(
					'values'		=> $valueInternacionais->returnValues('prices','coinbasepro','BTC/USD'),
					'book'			=> $valueInternacionais->returnValues('book','coinbasepro','BTC/USD'),
				)
			),
			'hitbtc'		=> array(
				'location'		=> 'global',
				'info'			=> parent::exchangeInfos(79,'internacionais'),
				'BTC'			=> array(
					'values'		=> $valueInternacionais->returnValues('prices','hitbtc','BTC/USDT'),
					'book'			=> $valueInternacionais->returnValues('book','hitbtc','BTC/USDT'),
				)
			),
			'huobi'		=> array(
				'location'		=> 'global',
				'info'			=> parent::exchangeInfos(80,'internacionais'),
				'BTC'			=> array(
					'values'		=> $valueInternacionais->returnValues('prices','huobipro','BTC/USDT'),
					'book'			=> $valueInternacionais->returnValues('book','huobipro','BTC/USDT'),
				)
			),
			
			'bitstamp'		=> array(
				'location'		=> 'global',
				'info'			=> parent::exchangeInfos(83,'internacionais'),
				'BTC'			=> array(
					'values'		=> $valueInternacionais->returnValues('prices','bitstamp','BTC/USD'),
					'book'			=> $valueInternacionais->returnValues('book','bitstamp','BTC/USD'),
				)
			),
			'bittrex'		=> array(
				'location'		=> 'global',
				'info'			=> parent::exchangeInfos(84,'internacionais'),
				'BTC'			=> array(
					'values'		=> $valueInternacionais->returnValues('prices','bittrex','BTC/USD'),
					'book'			=> $valueInternacionais->returnValues('book','bittrex','BTC/USD'),
				)
			),
			'gateio'		=> array(
				'location'		=> 'global',
				'info'			=> parent::exchangeInfos(573,'internacionais'),
				'BTC'			=> array(
					'values'		=> $valueInternacionais->returnValues('prices','gateio','BTC/USDT'),
					'book'			=> $valueInternacionais->returnValues('book','gateio','BTC/USDT'),
				)
			),
			'digifinex'		=> array(
				'location'		=> 'global',
				'info'			=> parent::exchangeInfos(569,'internacionais'),
				'BTC'			=> array(
					'values'		=> $valueInternacionais->returnValues('prices','digifinex','BTC/USDT'),
					'book'		=> $valueInternacionais->returnValues('book','digifinex','BTC/USDT'),
				)
			),
			
			/*
			'okex'		=> array(
				'location'		=> 'global',
				'info'			=> parent::exchangeInfos(76,'internacionais'),
				'BTC'			=> array(
					'values'		=> $valueInternacionais->returnValues('prices','okex','BTC/USDT'),
					//'book'		=> $valueInternacionais->returnValues('book','book','BTCUSDT'),
				)
			),
			*/
	    );
	}
}