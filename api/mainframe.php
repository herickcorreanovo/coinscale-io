<?php

header("Content-type:application/json");

function contructor()
{
	/* INSTACIA LIBRARIES*/

	$start = microtime(true);

	require_once 'controller.php';

	$exchanges = new Exchanges();

	/* ESCREVE FILE BR */

	$br_Exchanges = $exchanges->returned_BR_Exchanges();

	$fileBR = fopen('jsons/exchanges-trading-br.json','w+');
	fwrite( $fileBR , json_encode($br_Exchanges) );
	fclose( $fileBR );

	$brExchanges = array(
		'time'		=> $time_elapsed_secs = microtime(true) - $start,
		'exchanges' => $br_Exchanges
	);

	/* ESCREVE FILE GLOBAL */

	$global_Exchanges = $exchanges->returned_Global_Exchanges();

	$fileGlobal = fopen('jsons/exchanges-trading-global.json','w+');
	fwrite( $fileGlobal , json_encode($global_Exchanges) );
	fclose( $fileGlobal );

	/* ESCREVE FILE ALL */

	$allExchanges = array(
		'time'		=> $time_elapsed_secs = microtime(true) - $start,
		'exchanges' => array_merge($global_Exchanges)
		//'exchanges' => array_merge($br_Exchanges, $global_Exchanges)
	);

	$fileAll = fopen('jsons/all-exchanges-trading.json','w+');
	fwrite( $fileAll , json_encode($allExchanges) );
	fclose( $fileAll );

	echo json_encode($allExchanges);
}

contructor();

/*
if( $_SERVER['SERVER_NAME'] == 'dev.coinscale.io' )
{
	contructor();
}

if( $_SERVER['SERVER_NAME'] == 'coinscale.io' )
{
	contructor();

	sleep(30);

	contructor();
}
*/