<?php

require_once 'csc-config.php';
require_once 'csc-sendmail.php';

$sendmail = new phpMailerClass();

class User extends DatabaseConnect
{
	function testeUser($data)
	{
		global $sendmail;
		
		//$json = file_get_contents('csc-sendmail.php?email="'.$data['email'].'"&nome="'.$data['firstName'].' '.$data['lastName'].'"&userToken="'.md5($data['email'].$data['CPF']).'"&action="newUser"');

		echo $sendmail->SendMail(
			array(
				'token' 	=> parent::Settings('token'),
				'action'	=> 'newUser',
				'email' 	=> $data['email'],
				'name' 		=> $data['firstName'].' '.$data['lastName'],
				'userToken' => md5($data['email'].$data['CPF']),
			)
		);

		$conn = parent::Settings('databaseOpen')['connectDB'];
		
		$sql = "select * from csc_users where email='".$data['email']."' OR cpf='".$data['CPF']."'";
		$result = $conn->query($sql);

		if($result->num_rows > 0)
		{
			$update = "UPDATE csc_users SET last_access='".date( 'Y-m-d:H-i-s',time() )."' WHERE email='".$data['email']."' AND passw='".$data['passw']."'";
			
		    $conn->query($update);
		    $conn->close();
		    return true;    
		}
		else
		{
			return false;
			$conn->close();
		}
	}

	function createUse($data)
	{
		$conn = parent::Settings('databaseOpen')['connectDB'];

		$register = "INSERT INTO csc_users
		(
			email,
			first_name,
			last_name,
			passw,
			gender,
			country,
			state,
			city,
			cpf,
			date_register,
			last_access
		)
		VALUES
		(
			'".$data['email']."',
			'".$data['firstName']."',
			'".$data['lastName']."',
			'".$data['passw']."',
			'".$data['gender']."',
			'".$data['country']."',
			'".$data['state']."',
			'".$data['city']."',
			'".$data['CPF']."',
			'".$data['dateReg']."',
			'".$data['dateReg']."'
		)";

		if ($conn->query($register) === TRUE)
		{
			$conn->close();
		    return true;
		}
		else
		{
			echo "Error updating record: " . $conn->error;
		}
	}

	function updateUser()
	{

	}

	function userControll($action,$token,$data)
	{
		if( $token == parent::Settings('token') )
		{
			switch ($action)
			{
				case 'login':
					if( $this->testeUser($data) == true )
					{
						return array(
							'status'=> true,
							'type'	=> 'login',
						);
					}
					else
					{
						return array(
							'status'=> false,
							'type'	=> 'login',
						);
					}
				break;

				case 'register':
					if( $this->testeUser($data) == true )
					{
						return array(
							'status'=> false,
							'type'	=> 'register',
							'error' => 'User already exsist'
						);
					}
					else
					{
						if( $this->createUse($data) == true )
						{
							return array(
								'status'=> true,
								'type'	=> 'register',
							);
						}
						else
						{
							return array(
								'status'=> false,
								'type'	=> 'register',
							);
						}
					}
				break;
			}
		}
		else
		{
			return array(
				'status'=> false,
				'type'	=> 'token',
				'token' => false,
			);
		}
	}
}