<?php

class DatabaseConnect
{
	function dataConnection()
	{
		return array(
		    'host' 	=> 'localhost',
		    'user'	=> 'root',
		    'passw' => '',
		    'base' 	=> 'coinscale_app',
		);
	}

	function openDB()
	{
		$conn = new mysqli(
			$this->dataConnection()['host'],
			$this->dataConnection()['user'],
			$this->dataConnection()['passw'],
			$this->dataConnection()['base']
		);

		if( $conn->connect_error )
		{
			return array(
				'status'=> false,
				'error' => "Failed to connect to MySQL: " . mysqli_connect_error(),
			);
		}
		else
		{
			return array(
				'status' 	=> true,
				'connectDB'	=> $conn,
			);
		}
	}

	function closeDB()
	{
		mysqli_close(
			mysqli_connect(
				$this->dataConnection()['host'],
				$this->dataConnection()['user'],
				$this->dataConnection()['passw'],
				$this->dataConnection()['base']
			)
		);
	}

	function Settings($action)
	{
		switch ($action)
		{
			case 'databaseOpen':
				return $this->openDB();
			break;
			case 'databaseClose':
				return $this->closeDB();
			break;
			case 'token':
				return 'cCOf3&]+1()I4rg@>R1AE%x_{|{?y@';
			break;
		}
	}
}