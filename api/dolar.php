<?php

if($_GET['token'] == 'FxQYhUmg6XpvtN5NsQ9PBZeP1rvKBiai')
{
	$json = json_decode(file_get_contents('https://economia.awesomeapi.com.br/all/USD-BRL'),true);

	$high 		= floatval( str_replace(',' , '.', $json['USD']['high']) );
	$low 		= floatval( str_replace(',' , '.', $json['USD']['low']) );
	$pctChange 	= floatval( str_replace(',' , '.', $json['USD']['pctChange']) );
	$media 		= ($high + $low) / 2;

	$dolar = array(
	    'high' 			=> floatval( number_format( $high , 3 ) ),
	    'low'    		=> floatval( number_format( $low , 3 ) ),
	    'pctChange'		=> $pctChange,
	    'media'    		=> floatval( number_format( $media , 3 ) ),
	);

	header("Content-type:application/json"); 
	echo json_encode($dolar);

	$fileDolar = fopen('jsons/valor-dolar.json','w+');
	fwrite( $fileDolar , json_encode($dolar) );
	fclose( $fileDolar );
}