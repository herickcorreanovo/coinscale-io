<?php

class CORS
{
	function domainsAllowed()
	{
		return array(
			'https://localhost',
			'https://localhost:3000',
			'https://coinscale.io',
			'https://dev.coinscale.io',
			'https://comparadores.webitcoin.com',
			'https://comparadores.webitcoin.com.br',
			'https://criptomoedas.webitcoin.com.br',
		);
	}
}