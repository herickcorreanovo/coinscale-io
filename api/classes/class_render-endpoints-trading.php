<?php

require_once 'class_render-errors.php';
require_once 'cors.php';

$cors = new CORS();

class RenderEndpointTrading extends RenderErrors
{
	function renderTradingLocation($route)
	{
		global $cors;
		global $rootPath;

		$result = [];

		if( isset($route[2]) && $route[2])
		{
			if( $route[2] == 'br' )
			{
				$json = json_decode(file_get_contents($rootPath.'/api/jsons/exchanges-trading-br.json'),true);
			}
			else
			{
				$json = json_decode(file_get_contents($rootPath.'/api/jsons/exchanges-trading-global.json'),true);
			}

			if( isset($route[3]) && $route[3] == 'cryptocurrency')
			{
				foreach ($json as $exchanges)
				{
					if($exchanges['location'] && $exchanges['location'] == $route[2])
					{
						
						if( isset($route[4]) && $route[4])
						{
							if( isset( $exchanges[ $route[4] ] ) )
							{
								$result[$exchanges['info']['nicename']] = $exchanges;
							}
						}
					}
				}
			}
			else
			{
				foreach ($json as $exchanges)
				{
					if($exchanges['location'] && $exchanges['location'] == $route[2])
					{
						$result[$exchanges['info']['nicename']] = $exchanges;
					}
				}
			}

			if($result)
			{
				if( isset($_SERVER['HTTP_ORIGIN']) )
				{
					if ( in_array($_SERVER['HTTP_ORIGIN'], $cors->domainsAllowed()) )
					{
					    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
					}
				}
				else
				{
					header("Content-type:application/json"); 
				}
				echo json_encode($result);
			}
			else
			{
				echo parent::errorHTML("The location or cryptocurrency didn't exists in our system");
			}
		}
		else
		{
			echo parent::errorHTML("The location or cryptocurrency is required");			
		}
	}

	function renderTradingCrypto($route)
	{
		global $cors;
		global $rootPath;
		
		$json = json_decode(file_get_contents($rootPath.'/api/jsons/all-exchanges-trading.json'),true);

		$result = [];

		if( isset($route[2]) && $route[2])
		{
			foreach ($json['exchanges'] as $exchanges)
			{
				if( isset( $exchanges[ $route[2] ] ) )
				{
					$result[$exchanges['info']['nicename']] = $exchanges;
				}
			}

			if($result)
			{
				if( isset($_SERVER['HTTP_ORIGIN']) )
				{
					if ( in_array($_SERVER['HTTP_ORIGIN'], $cors->domainsAllowed()) )
					{
					    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
					}
				}
				else
				{
					header("Content-type:application/json"); 
				}
				echo json_encode($result);
			}
			else
			{
				echo parent::errorHTML("The cryptocurrency didn't exists in our system yet");
			}
		}
		else
		{
			echo parent::errorHTML("The cryptocurrency is required");			
		}
	}

	function RenderTradingEndpoint($route)
	{
		switch($route[1])
		{
			case 'cryptocurrency':
				$this->renderTradingCrypto($route);
			break;

			default:
				$this->renderTradingLocation($route);
			break;
		}	
	}
}