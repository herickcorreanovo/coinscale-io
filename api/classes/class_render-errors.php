<?php
class RenderErrors
{
	function errorHTML($error)
	{
		$html = '<!DOCTYPE html>';
		$html .= '<html lang="en">';
		$html .= '<head>';
			$html .= '<meta charset="UTF-8">';
			$html .= '<title>Erro de Conexão - coinscale.io</title>';
		$html .= '</head>';
		$html .= '<body>';
			$html .= '<h1>'.$error.'</h1>';
		$html .= '</body>';
		$html .= '</html>';

		return $html;
	}	
}