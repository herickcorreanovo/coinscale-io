<?php

require_once 'class_money-convertion.php';

class BookCalcs extends MoneyConvertion
{
	function calcBookBR($params)
    {
        //print_r($params);

        $bookQtdeBid = 1;
        $bookPriceBid = 0;

        $bookQtdeAsk = 0;
        $bookPriceAsk = 0;

        $testeQtd = array();
        $testePrice = array();

        foreach ($params['bids'] as $ordens)
        {
            $price = floatval( $ordens[ $params['bid_price'] ] );
            $qtde = floatval( $ordens[ $params['bid_qtd'] ] );
            
            if($bookQtdeBid > 0)
            {
                $bookQtdeBid -= $qtde;
                $bookPriceBid = $price;
            }
        }

        foreach ($params['asks'] as $ordens)
        {
            $price = floatval( $ordens[ $params['ask_price'] ] );
            $qtde = floatval( $ordens[ $params['ask_qtd'] ] );
            
            if($bookQtdeAsk < 1)
            {
                $bookQtdeAsk += $qtde;
                $bookPriceAsk = $price;
            }
        };

        if($params['dolar'] == 'toDolar')
        {
            return array(
                'br_buy'    => number_format($bookPriceAsk, 2, ',', '.'),
                'br_sell'   => number_format($bookPriceBid, 2, ',', '.'),
                'dolar_buy' => number_format( parent::moneyConvert('toDolar', $bookPriceAsk), 2, '.', ','),
                'dolar_sell'=> number_format( parent::moneyConvert('toDolar', $bookPriceBid), 2, '.', ','),
            );
        }
        else
        {
            return array(
                'br_buy'    => number_format( parent::moneyConvert('toReal', $bookPriceAsk), 2, ',', '.'),
                'br_sell'   => number_format( parent::moneyConvert('toReal', $bookPriceBid), 2, ',', '.'),
                'dolar_buy' => number_format( $bookPriceAsk, 2, '.', ','),
                'dolar_sell'=> number_format( $bookPriceBid, 2, '.', ','),
            );
        }
    }
}