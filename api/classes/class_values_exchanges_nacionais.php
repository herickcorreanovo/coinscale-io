<?php

require_once 'ccxt/ccxt.php';
require_once 'class_book_calcs.php';

class ValuesExchangesNacionais extends BookCalcs
{
	function mercadobitcoin($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$json = json_decode(file_get_contents('https://www.mercadobitcoin.net/api/'.$coin.'/ticker/'),true);

			return array(
			    'buy' 			=> number_format($json['ticker']['buy'], 2, ',', '.'),
	            'sell'    		=> number_format($json['ticker']['sell'], 2, ',', '.'),
	            'last'    		=> number_format($json['ticker']['last'], 2, ',', '.'),
	            'high'    		=> number_format($json['ticker']['high'], 2, ',', '.'),
	            'low'     		=> number_format($json['ticker']['low'], 2, ',', '.'),
	            'volume'  		=> number_format( $json['ticker']['vol'], 2 ),
	            'percentChange' => false,
	        );
		}
		else
		{
			$json = json_decode(file_get_contents('https://www.mercadobitcoin.net/api/'.$coin.'/orderbook/'),true);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json['bids'],
					'bid_price' => 0,
					'bid_qtd' 	=> 1,
					'asks'	=> $json['asks'],
					'ask_price' => 0,
					'ask_qtd' 	=> 1,
					'dolar'	=> 'toDolar',
				)
			);
		}
	}

	function bitcointrade($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$json = json_decode(file_get_contents('https://api.bitcointrade.com.br/v2/public/'.$coin.'/ticker/'),true);

			return array(
			    'buy' 			=> number_format($json['data']['buy'], 2, ',', '.'),
	            'sell'    		=> number_format($json['data']['sell'], 2, ',', '.'),
	            'last'    		=> number_format($json['data']['last'], 2, ',', '.'),
	            'high'    		=> number_format($json['data']['high'], 2, ',', '.'),
	            'low'     		=> number_format($json['data']['low'], 2, ',', '.'),
	            'volume'  		=> number_format( $json['data']['volume'], 2),
	            'percentChange' => false,
			);
		}
		else
		{
			$json = json_decode(file_get_contents('https://api.bitcointrade.com.br/v2/public/'.$coin.'/orders/'),true);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json['data']['bids'],
					'bid_price' => 'unit_price',
					'bid_qtd' 	=> 'amount',
					'asks'	=> $json['data']['asks'],
					'ask_price' => 'unit_price',
					'ask_qtd' 	=> 'amount',
					'dolar'	=> 'toDolar',
				)
			);
		}
	}

	function bitcambio($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$json = json_decode(file_get_contents('https://bitcambio_api.blinktrade.com/api/v1/BRL/ticker?crypto_currency='.$coin),true);

			return array(
			    'buy' 			=> number_format($json['buy'], 2, ',', '.'),
	            'sell'    		=> number_format($json['sell'], 2, ',', '.'),
	            'last'    		=> number_format($json['last'], 2, ',', '.'),
	            'high'    		=> number_format($json['high'], 2, ',', '.'),
	            'low'     		=> number_format($json['low'], 2, ',', '.'),
	            'volume'  		=> number_format( $json['vol'], 2 ),
	            'percentChange' => false,
			);
		}
		else
		{
			$json = json_decode(file_get_contents('https://bitcambio_api.blinktrade.com/api/v1/BRL/orderbook?crypto_currency='.$coin),true);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json['bids'],
					'bid_price' => 0,
					'bid_qtd' 	=> 1,
					'asks'	=> $json['asks'],
					'ask_price' => 0,
					'ask_qtd' 	=> 1,
					'dolar'	=> 'toDolar',
				)
			);
		}
	}

	function bitpreco($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$context = stream_context_create(
				array(
					"http" => array(
						"header" => "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36"
					)
				)
			);

			$json = json_decode(file_get_contents('https://api.bitpreco.com/'.$coin.'/ticker', false, $context),true);

			return array(
			    'buy' 			=> null,
	            'sell'    		=> null,
	            'last'    		=> number_format($json['last'], 2, ',', '.'),
	            'high'    		=> number_format($json['high'], 2, ',', '.'),
	            'low'     		=> number_format($json['low'], 2, ',', '.'),
	            'volume'  		=> number_format( $json['vol'], 2 ),
	            'percentChange' => intval($json['var']),
			);
		}
		else
		{
			$context = stream_context_create(
				array(
					"http" => array(
						"header" => "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36"
					)
				)
			);

			$json = json_decode(file_get_contents('https://api.bitpreco.com/'.$coin.'/orderbook', false, $context),true);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json['bids'],
					'bid_price' => 'price',
					'bid_qtd' 	=> 'amount',
					'asks'	=> $json['asks'],
					'ask_price' => 'price',
					'ask_qtd' 	=> 'amount',
					'dolar'	=> 'toDolar',
				)
			);
		}
	}

	function bitblue($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$json = json_decode(file_get_contents('https://bitblue.com/api/stats'),true);

			return array(
			    'buy' 			=> number_format($json['stats']['bid'], 2, ',', '.'),
	            'sell'    		=> number_format($json['stats']['ask'], 2, ',', '.'),
	            'last'    		=> number_format($json['stats']['last_price'], 2, ',', '.'),
	            'high'    		=> number_format($json['stats']['max'], 2, ',', '.'),
	            'low'     		=> number_format($json['stats']['min'], 2, ',', '.'),
	            'volume'  		=> number_format( $json['stats']['24h_volume'], 2 ),
	            'percentChange' => intval($json['stats']['daily_change_percent']),
			);
		}
		else
		{
			$json = json_decode(file_get_contents('https://bitblue.com/api/order-book'),true);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json['order-book']['bid'],
					'bid_price' => 'price',
					'bid_qtd' 	=> 'order_amount',
					'asks'	=> $json['order-book']['ask'],
					'ask_price' => 'price',
					'ask_qtd' 	=> 'order_amount',
					'dolar'	=> 'toDolar',
				)
			);
		}
	}

	function bitcointoyou($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$json = json_decode(file_get_contents('https://api_v1.bitcointoyou.com/'.$coin),true);

			return array(
			    'buy' 			=> number_format($json['ticker']['buy'], 2, ',', '.'),
	            'sell'    		=> number_format($json['ticker']['sell'], 2, ',', '.'),
	            'last'    		=> number_format($json['ticker']['last'], 2, ',', '.'),
	            'high'    		=> number_format($json['ticker']['high'], 2, ',', '.'),
	            'low'     		=> number_format($json['ticker']['low'], 2, ',', '.'),
	            'volume'  		=> number_format( $json['ticker']['vol'], 2 ),
	            'percentChange' => false,
			);
		}
		else
		{
			$json = json_decode(file_get_contents('https://api_v1.bitcointoyou.com/'.$coin),true);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json['bids'],
					'bid_price' => 0,
					'bid_qtd' 	=> 1,
					'asks'	=> $json['asks'],
					'ask_price' => 0,
					'ask_qtd' 	=> 1,
					'dolar'	=> 'toDolar',
				)
			);
		}
	}

	function novadax($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$json = json_decode(file_get_contents('https://api.novadax.com/v1/market/ticker?symbol='.$coin),true);

			return array(
			    'buy' 			=> number_format($json['data']['bid'], 2, ',', '.'),
	            'sell'    		=> number_format($json['data']['ask'], 2, ',', '.'),
	            'last'    		=> number_format($json['data']['lastPrice'], 2, ',', '.'),
	            'high'    		=> number_format($json['data']['high24h'], 2, ',', '.'),
	            'low'     		=> number_format($json['data']['low24h'], 2, ',', '.'),
	            'volume'  		=> number_format( $json['data']['baseVolume24h'], 2 ),
	            'percentChange' => false,
			);
		}
		else
		{
			$json = json_decode(file_get_contents('https://api.novadax.com/v1/market/depth?symbol='.$coin.'&size=20'),true);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json['data']['bids'],
					'bid_price' => 0,
					'bid_qtd' 	=> 1,
					'asks'	=> $json['data']['asks'],
					'ask_price' => 0,
					'ask_qtd' 	=> 1,
					'dolar'	=> 'toDolar',
				)
			);
		}
	}

	function brasiliex($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$json = json_decode(file_get_contents('https://braziliex.com/api/v1/public/ticker'),true);

			return array(
			    'buy' 			=> number_format($json[$coin]['highestBid'], 2, ',', '.'),
	            'sell'    		=> number_format($json[$coin]['lowestAsk'], 2, ',', '.'),
	            'last'    		=> number_format($json[$coin]['last'], 2, ',', '.'),
	            'high'    		=> number_format($json[$coin]['highestBid24'], 2, ',', '.'),
	            'low'     		=> number_format($json[$coin]['lowestAsk24'], 2, ',', '.'),
	            'volume'  		=> number_format( $json[$coin]['baseVolume24'], 2 ),
	            'percentChange' => number_format($json[$coin]['percentChange']),
			);
		}
		else
		{
			$json = json_decode(file_get_contents('https://braziliex.com/api/v1/public/orderbook/'.$coin),true);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json['bids'],
					'bid_price' => 'price',
					'bid_qtd' 	=> 'amount',
					'asks'	=> $json['asks'],
					'ask_price' => 'price',
					'ask_qtd' 	=> 'amount',
					'dolar'	=> 'toDolar',
				)
			);
		}
	}

	function bitnuvem($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$json = json_decode(file_get_contents('https://bitnuvem.com/api/'.$coin.'/ticker'),true);

			return array(
			    'buy' 			=> number_format($json['ticker']['buy'], 2, ',', '.'),
	            'sell'    		=> number_format($json['ticker']['sell'], 2, ',', '.'),
	            'last'    		=> number_format($json['ticker']['last'], 2, ',', '.'),
	            'high'    		=> number_format($json['ticker']['high'], 2, ',', '.'),
	            'low'     		=> number_format($json['ticker']['low'], 2, ',', '.'),
	            'volume'  		=> number_format($json['ticker']['vol'], 2),
	            'percentChange' => false,
			);
		}
		else
		{
			$json = json_decode(file_get_contents('https://bitnuvem.com/api/'.$coin.'/orderbook'),true);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json['bids'],
					'bid_price' => 0,
					'bid_qtd' 	=> 1,
					'asks'	=> $json['asks'],
					'ask_price' => 0,
					'ask_qtd' 	=> 1,
					'dolar'	=> 'toDolar',
				)
			);
		}
	}

	function pagcripto($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$json = json_decode(file_get_contents('https://api.pagcripto.com.br/v1/'.$coin.'/ticker'),true);

			return array(
			    'buy' 			=> number_format($json['data']['buy'], 2, ',', '.'),
	            'sell'    		=> number_format($json['data']['sell'], 2, ',', '.'),
	            'last'    		=> number_format($json['data']['last'], 2, ',', '.'),
	            'high'    		=> number_format($json['data']['high'], 2, ',', '.'),
	            'low'     		=> number_format($json['data']['low'], 2, ',', '.'),
	            'volume'  		=> number_format( $json['data']['volume'], 2 ),
	            'percentChange' => false,
			);
		}
		else
		{
			$json = json_decode(file_get_contents('https://api.pagcripto.com.br/v1/'.$coin.'/orders'),true);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json['data']['bids'],
					'bid_price' => 'cot',
					'bid_qtd' 	=> 'qtd',
					'asks'	=> $json['data']['asks'],
					'ask_price' => 'cot',
					'ask_qtd' 	=> 'qtd',
					'dolar'	=> 'toDolar',
				)
			);
		}
	}

	function omnitrade($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$json = json_decode(file_get_contents('https://omnitrade.io/api/v2/tickers'),true);

			return array(
			    'buy' 			=> number_format($json[$coin]['ticker']['buy'], 2, ',', '.'),
	            'sell'    		=> number_format($json[$coin]['ticker']['sell'], 2, ',', '.'),
	            'last'    		=> number_format($json[$coin]['ticker']['last'], 2, ',', '.'),
	            'high'    		=> number_format($json[$coin]['ticker']['high'], 2, ',', '.'),
	            'low'     		=> number_format($json[$coin]['ticker']['low'], 2, ',', '.'),
	            'volume'  		=> number_format( $json[$coin]['ticker']['vol'], 2 ),
	            'percentChange' => false,
			);
		}
		else
		{
			$json = json_decode(file_get_contents('https://omnitrade.io/api/v2/orderbook?market='.$coin),true);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json['bids'],
					'bid_price' => 0,
					'bid_qtd' 	=> 1,
					'asks'	=> $json['asks'],
					'ask_price' => 0,
					'ask_qtd' 	=> 1,
					'dolar'	=> 'toDolar',
				)
			);
		}
	}

	function bitrecife($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$json = json_decode(file_get_contents('https://exchange.bitrecife.com.br/api/v3/public/getmarketsummaries'),true);

			foreach($json['result'] as $results)
			{
				if($results['MarketName'] == $coin)
				{
					return array(
						'buy' 			=> number_format($results['Bid'], 2, ',', '.'),
						'sell'    		=> number_format($results['Ask'], 2, ',', '.'),
						'last'    		=> number_format($results['Last'], 2, ',', '.'),
						'high'    		=> number_format($results['High'], 2, ',', '.'),
						'low'     		=> number_format($results['Low'], 2, ',', '.'),
						'volume'  		=> number_format( $results['Volume'], 2 ),
						'percentChange' => number_format( intval($results['Average']) / 10000 ),
					);
				}
			}
		}
		else
		{
			$json = json_decode(file_get_contents('https://exchange.bitrecife.com.br/api/v3/public/getorderbook?market='.$coin.'&type=ALL&depth=40'),true);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json['result']['buy'],
					'bid_price' => 'Rate',
					'bid_qtd' 	=> 'Quantity',
					'asks' 	=> $json['result']['sell'],
					'ask_price' => 'Rate',
					'ask_qtd' 	=> 'Quantity',
					'dolar'	=> 'toDolar',
				)
			);
		}
	}

	function profitfy($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$json = json_decode(file_get_contents('https://profitfy.trade/api/v1/public/ticker/'.$coin.'/BRL'),true);

			return array(
			    'buy' 			=> number_format($json[0]['buy'], 2, ',', '.'),
	            'sell'    		=> number_format($json[0]['sell'], 2, ',', '.'),
	            'last'    		=> number_format($json[0]['last'], 2, ',', '.'),
	            'high'    		=> number_format($json[0]['max'], 2, ',', '.'),
	            'low'     		=> number_format($json[0]['min'], 2, ',', '.'),
	            'volume'  		=> number_format( $json[0]['volume'], 2 ),
	            'percentChange' => false,
			);
		}
		else
		{
			$json = json_decode(file_get_contents('https://profitfy.trade/api/v1/public/orderbook/'.$coin),true);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json[0]['buy'],
					'bid_price' => 'price',
					'bid_qtd' 	=> 'amount',
					'asks' 	=> $json[0]['sell'],
					'ask_price' => 'price',
					'ask_qtd' 	=> 'amount',
					'dolar'	=> 'toDolar',
				)
			);
		}
	}










	/*
	function foxbitTicker($kindOfValues,$coin)
	{
		if($kindOfValues == 'prices')
		{
			$ws = new ws(array
			(
				'host' => 'wss://api.foxbitapi.com.br/WSGateway/',
				'port' => 443,
				'path' => ''
			));
			$result = $ws->send('message');
			$ws->close();
			echo $result;

			$foxbitClass = $bitmex = new \ccxt\foxbit();

			//$symbols2 = array_keys ($foxbitClass->markets);

			//$foxbitClass->fetchL2OrderBook("BTC/BRL");

			echo '<pre>';
			print_r( $foxbitClass->fetchL2OrderBook("BTC/BRL") );
			echo '</pre>';

			print_r($foxbitClass->has);
		}
		else
		{}
	}

	function walltime()
	{
		$date = date_create();

		$json = json_decode(file_get_contents('https://s3.amazonaws.com/data-production-walltime-info/production/dynamic/walltime-info.json?now='.date_timestamp_get($date)),true);

		return array(
		    'buy' 			=> number_format($json['BRL_XBT']['highest_bid_inexact'], 2, ',', '.'),
            'sell'    		=> number_format($json['BRL_XBT']['lowest_ask_inexact'], 2, ',', '.'),
            'last'    		=> number_format($json['BRL_XBT']['last_inexact'], 2, ',', '.'),
            'high'    		=> null,
            'low'     		=> null,
            'volume'  		=> number_format( $json['BRL_XBT']['quote_volume24h_inexact'], 2 ),
            'percentChange' => false,
		);
	}

	function modiax($coin)
	{
		$json = json_decode(file_get_contents('https://app.modiax.com/api/v2/tickers'),true);

		return array(
		    'buy' 			=> number_format($json[$coin]['ticker']['buy'], 2, ',', '.'),
            'sell'    		=> number_format($json[$coin]['ticker']['sell'], 2, ',', '.'),
            'last'    		=> number_format($json[$coin]['ticker']['last'], 2, ',', '.'),
            'high'    		=> number_format($json[$coin]['ticker']['high'], 2, ',', '.'),
            'low'     		=> number_format($json[$coin]['ticker']['low'], 2, ',', '.'),
            'volume'  		=> number_format( $json[$coin]['ticker']['vol'], 2 ),
            'percentChange' => false,
		);
	}
	*/
	

	/*
	function negociecoins($coin)
	{
		$json = json_decode(file_get_contents('https://broker.negociecoins.com.br/api/v3/'.$coin.'/ticker'),true);

		return array(
		    'buy' 			=> intval($json['buy']),
            'sell'    		=> intval($json['sell']),
            'last'    		=> intval($json['last']),
            'high'    		=> intval($json['high']),
            'low'     		=> intval($json['low']),
            'volume'  		=> intval($json['vol']),
            'percentChange' => '',
		);
	}

	function tembtc($coin)
	{
		$json = json_decode(file_get_contents('https://broker.tembtc.com.br/api/v3/'.$coin.'/ticker'),true);

		return array(
		    'buy' 			=> intval($json['buy']),
            'sell'    		=> intval($json['sell']),
            'last'    		=> intval($json['last']),
            'high'    		=> intval($json['high']),
            'low'     		=> intval($json['low']),
            'volume'  		=> intval($json['vol']),
            'percentChange' => '',
		);
	}

	function threexbit($coin)
	{
		$json = json_decode(file_get_contents('https://api.exchange.3xbit.com.br/ticker/'.$coin),true);

		return array(
		    'buy' 			=> number_format($json['CREDIT_BTC']['bid'], 2, ',', '.'),
            'sell'    		=> number_format($json['CREDIT_BTC']['ask'], 2, ',', '.'),
            'last'    		=> number_format($json['CREDIT_BTC']['last'], 2, ',', '.'),
            'high'    		=> number_format($json['CREDIT_BTC']['max'], 2, ',', '.'),
            'low'     		=> number_format($json['CREDIT_BTC']['min'], 2, ',', '.'),
            'volume'  		=> number_format( $json['CREDIT_BTC']['volume'], 2 ),
            'percentChange' => intval($json['CREDIT_BTC']['variation']),
		);
	}
	*/
}