<?php

require_once 'class_render-errors.php';
require_once 'cors.php';

class RenderEndpointAccess extends RenderErrors
{
	function renderAccess($route)
	{
		global $cors;
		global $rootPath;
		
		if( isset($route[1]) && $route[1])
		{
			if($route[1] == 'criteria')
			{
				$json = json_decode(file_get_contents($rootPath.'/api/jsons/criterios_similar.json'),true);

				if( isset($_SERVER['HTTP_ORIGIN']) )
				{
					if ( in_array($_SERVER['HTTP_ORIGIN'], $cors->domainsAllowed()) )
					{
					    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
					}
				}
				else
				{
					header("Content-type:application/json"); 
				}
				echo json_encode($json);
			}
			else
			{
				if($route[1] == 'br')
				{
					$json = json_decode(file_get_contents($rootPath.'/api/jsons/api-similarweb-nacionais.json'),true);

					if( isset($_SERVER['HTTP_ORIGIN']) )
					{
						if ( in_array($_SERVER['HTTP_ORIGIN'], $cors->domainsAllowed()) )
						{
						    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
						}
					}
					else
					{
						header("Content-type:application/json"); 
					}
					echo json_encode($json);
				}
				else if($route[1] == 'global')
				{
					$json = json_decode(file_get_contents($rootPath.'/api/jsons/api-similarweb-internacionais.json'),true);

					if( isset($_SERVER['HTTP_ORIGIN']) )
					{
						if ( in_array($_SERVER['HTTP_ORIGIN'], $cors->domainsAllowed()) )
						{
						    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
						}
					}
					else
					{
						header("Content-type:application/json"); 
					}
					echo json_encode($json);
				}
				else
				{
					echo parent::errorHTML("The location stil didn't exists in our system");
				}
			}
		}
		else
		{
			echo parent::errorHTML("The location is required");
		}
	}

	function RenderAccessEndpoint($route,$token,$url)
	{
		$this->renderAccess($route);
	}
}