<?php

require_once 'class_render-errors.php';
require_once 'cors.php';

class RenderEndpointFee extends RenderErrors
{
	function renderFeeLocation($route)
	{
		global $cors;
		global $rootPath;

		$json = json_decode(file_get_contents($rootPath.'/api/jsons/info_exchanges.json'),true);

		$result = [];

		if( isset($route[2]) && $route[2])
		{
			if( isset($route[3]) && $route[3] == 'cyptocurrency')
			{
				foreach ($json as $exchanges)
				{
					if($exchanges['location'] && $exchanges['location'] == $route[2])
					{
						if( isset($route[4]) && $route[4])
						{
							if( isset($exchanges['fee']) && isset( $exchanges['fee'][$route[4] ] ) )
							{
								foreach($exchanges['fee'] as $key => $teste)
								{
									if( $key != $route[4])
									{
										unset( $exchanges['fee'][$key] );
									}
								}

								$result[$exchanges['nicename']] = $exchanges;
							}
						}
					}
				}
			}
			else
			{
				foreach ($json as $exchanges)
				{
					if($exchanges['location'] && $exchanges['location'] == $route[2])
					{
						$result[$exchanges['nicename']] = $exchanges;
					}
				}
			}

			if($result)
			{
				if( isset($_SERVER['HTTP_ORIGIN']) )
				{
					if ( in_array($_SERVER['HTTP_ORIGIN'], $cors->domainsAllowed()) )
					{
					    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
					}
				}
				else
				{
					header("Content-type:application/json"); 
				}
				echo json_encode($result);
			}
			else
			{
				echo parent::errorHTML("The location or cryptocurrency didn't exists in our system");
			}
		}
		else
		{
			echo parent::errorHTML("The location is required");			
		}
	}

	function renderFeeCripto($route)
	{
		global $cors;
		global $rootPath;

		$json = json_decode(file_get_contents($rootPath.'/api/jsons/info_exchanges.json'),true);

		$result = [];

		if( isset($route[2]) && $route[2])
		{
			foreach ($json as $exchanges)
			{
				if( isset($exchanges['fee']) && isset( $exchanges['fee'][$route[2]] ) )
				{
					foreach($exchanges['fee'] as $key => $teste)
					{
						if( $key != $route[2])
						{
							unset( $exchanges['fee'][$key] );
						}
					}
					$result[$exchanges['nicename']] = $exchanges;
				}
			}

			if($result)
			{
				if( isset($_SERVER['HTTP_ORIGIN']) )
				{
					if ( in_array($_SERVER['HTTP_ORIGIN'], $cors->domainsAllowed()) )
					{
					    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
					}
				}
				else
				{
					header("Content-type:application/json"); 
				}
				echo json_encode($result);
			}
			else
			{
				echo parent::errorHTML("This cryptocurrency didn't exists in our system");
			}
		}
		else
		{
			echo parent::errorHTML("Some cryptocurrency is required");
		}
	}

	function renderFeeExchange($route)
	{
		global $cors;
		global $rootPath;
		
		$json = json_decode(file_get_contents($rootPath.'/api/jsons/info_exchanges.json'),true);

		$result = [];

		if( isset($route[2]) && $route[2])
		{
			foreach ($json as $exchanges)
			{
				if( isset($exchanges['nicename']) && $exchanges['nicename'] == $route[2] )
				{
					$result[$exchanges['nicename']] = $exchanges;
				}
			}
		}

		if($result)
		{
			if( isset($_SERVER['HTTP_ORIGIN']) )
			{
				if ( in_array($_SERVER['HTTP_ORIGIN'], $cors->domainsAllowed()) )
				{
				    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
				}
			}
			else
			{
				header("Content-type:application/json"); 
			}
			echo json_encode($result);
		}
		else
		{
			echo parent::errorHTML("This exchange didn't exists in our system");
		}
	}

	function renderCriterios()
	{
		global $cors;
		global $rootPath;

		$json = json_decode(file_get_contents($rootPath.'/api/jsons/criterios_taxas.json'),true);

		if( isset($_SERVER['HTTP_ORIGIN']) )
		{
			if ( in_array($_SERVER['HTTP_ORIGIN'], $cors->domainsAllowed()) )
			{
			    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
			}
		}
		else
		{
			header("Content-type:application/json"); 
		}
		echo json_encode($json);
	}

	function RenderFeeEndpoint($route)
	{
		switch($route[1])
		{
			case 'cryptocurrency':
				$this->renderFeeCripto($route);
			break;

			case 'exchange':
				$this->renderFeeExchange($route);
			break;

			case 'criteria':
				$this->renderCriterios();
			break;

			default:
				$this->renderFeeLocation($route);
			break;
		}
	}
}