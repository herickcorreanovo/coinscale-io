<?php
class MoneyConvertion
{
	function ConvertToDolar($price)
	{
		if($_SERVER['HTTP_HOST'] == 'coinscale.io')
		{
		    $rootPath = $_SERVER['DOCUMENT_ROOT'];
		}
		else
		{
		    $rootPath = 'https://coinscale.io';
		}

		$json = json_decode(file_get_contents($rootPath.'/api/jsons/valor-dolar.json'),true);

		$newPrice = str_replace(',' , '', number_format( $price / $json['media'], 2 ) );

		return floatval($newPrice);
	}

	function ConvertToReal($price)
	{
		if($_SERVER['HTTP_HOST'] == 'coinscale.io')
		{
		    $rootPath = $_SERVER['DOCUMENT_ROOT'];
		}
		else
		{
		    $rootPath = 'https://coinscale.io';
		}

		$json = json_decode(file_get_contents($rootPath.'/api/jsons/valor-dolar.json'),true);

		$newPrice = str_replace(',' , '', number_format( $json['media'] * $price, 2 ) );
		
		return floatval($newPrice);
	}

	function moneyConvert($money,$price)
	{
		switch($money)
		{
			case 'toDolar':
				return $this->ConvertToDolar($price);
			break;
			case 'toReal':
				return $this->ConvertToReal($price);
			break;
		}
	}
}