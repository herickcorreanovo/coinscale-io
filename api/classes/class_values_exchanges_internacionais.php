<?php

require_once 'ccxt/ccxt.php';
require_once 'class_book_calcs.php';

class ValuesExchangesInternacionais extends BookCalcs
{
	function cleanArrayInfo($dataResult)
	{
		$arrInfo = $dataResult;
		unset($arrInfo['info']);
		return $arrInfo;
	}

	function returnValues($kindOfValues,$exchange,$coin)
	{	
		switch ($exchange)
		{
			case 'binance':		$thisExchnage = new \ccxt\binance();		break;
			case 'bitmex':		$thisExchnage = new \ccxt\bitmex();			break;
			case 'kraken':		$thisExchnage = new \ccxt\kraken();			break;
			case 'bitfinex':	$thisExchnage = new \ccxt\bitfinex();		break;
			case 'okex':		$thisExchnage = new \ccxt\okex3();			break;
			case 'poloniex':	$thisExchnage = new \ccxt\poloniex();		break;
			case 'coinbasepro':	$thisExchnage = new \ccxt\coinbasepro();	break;
			case 'hitbtc':		$thisExchnage = new \ccxt\hitbtc();			break;
			case 'huobipro':	$thisExchnage = new \ccxt\huobipro();		break;
			case 'bitstamp':	$thisExchnage = new \ccxt\bitstamp();		break;
			case 'bittrex':		$thisExchnage = new \ccxt\bittrex();		break;
			case 'gateio':		$thisExchnage = new \ccxt\gateio();			break;
			case 'digifinex':
				$thisExchnage = new \ccxt\digifinex(
					array(
					    'apiKey' => '15dd1b36032eaf',
					    'secret' => 'c79b28e38e89f250fd2ab763474dc15f05dd1b360',
					)
				);
			break;
		}

		if($kindOfValues == 'prices')
		{
			$json = $this->cleanArrayInfo( $thisExchnage->fetchTicker($coin) );

			return array(
			    'buy' 			=> number_format($json['bid'], 2, ',', '.'),
	            'high'    		=> number_format($json['high'], 2, ',', '.'),
	            'low'     		=> number_format($json['low'], 2, ',', '.'),
	            'sell'    		=> number_format($json['ask'], 2, ',', '.'),
	            'last'    		=> number_format($json['last'], 2, ',', '.'),
	            'volume'  		=> number_format($json['baseVolume'], 2, ',', '.'),
	            'percentChange'	=> floatval($json['percentage']),
			);

			//sleep(1);
		}
		else
		{
			$json = $thisExchnage->fetchOrderBook($coin);

			return parent::calcBookBR(
				array(
					'bids' 	=> $json['bids'],
					'bid_price' => 0,
					'bid_qtd' 	=> 1,
					'asks' 	=> $json['asks'],
					'ask_price' => 0,
					'ask_qtd' 	=> 1,
					'dolar'	=> 'toReal',
				)
			);
			//sleep(1);
		}
	}
}