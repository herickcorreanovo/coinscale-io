<?php

require_once 'class_render-errors.php';
require_once 'cors.php';

class RenderEndpointExchanges extends RenderErrors
{
	function renderExchange($route)
	{
		global $cors;
		global $rootPath;
		
		if( isset($route[1]) )
		{
			/* INFO E TAXAS */

			$json = json_decode(file_get_contents($rootPath.'/api/jsons/info_exchanges.json'),true);

			$result = [];

			$id = '';
			$key = '';

			foreach ($json as $exchanges)
			{
				if($exchanges['nicename'] == $route[1])
				{
					$id = $exchanges['id'];
					$key = $exchanges['nicename'];

					if($exchanges)
					{
						$result[ $key ] = $exchanges;
					}
				}
			}

			/* ACESSOS */

			$jsonAcessosNacionais = json_decode(file_get_contents($rootPath.'/api/jsons/api-similarweb-nacionais.json'),true);
			$jsonAcessosInternacionais = json_decode(file_get_contents($rootPath.'/api/jsons/api-similarweb-internacionais.json'),true);
			$arrAcessos = array_merge($jsonAcessosNacionais, $jsonAcessosInternacionais);

			$notaAcessos = [];

			foreach ( $arrAcessos as $acessos )
			{
				if( isset( $acessos['id'] ) )
				{
					if( $acessos['id'] == $id )
					{
						$notaAcessos['notas'] = $acessos['notas'];
					}
				}
			}

			/* VALORES */

			$precos = json_decode(file_get_contents($rootPath.'/api/jsons/all-exchanges-trading.json'),true);
			
			unset($precos['exchanges'][$key]['location']);
			unset($precos['exchanges'][$key]['info']);
			unset($precos['exchanges'][$key]['calc']);

			$bookPrices['prices'] = $precos['exchanges'][$key];

			/* RENDER FINAL */

			$finalResult = array_merge($result[ $key ], $notaAcessos, $bookPrices);

			if( $finalResult )
			{
				if( isset($_SERVER['HTTP_ORIGIN']) )
				{
					if ( in_array($_SERVER['HTTP_ORIGIN'], $cors->domainsAllowed()) )
					{
					    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
					}
				}
				else
				{
					header("Content-type:application/json"); 
				}
				echo json_encode($finalResult);
			}
			else
			{
				echo parent::errorHTML("This exchange stil didn't exists in our system");
			}
		}
		else
		{
			echo parent::errorHTML("The nicename of exchange is required");
		}
	}

	function RenderExchangeEndpoint($route)
	{
		$this->renderExchange($route);
	}
}