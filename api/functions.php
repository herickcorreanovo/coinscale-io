<?php

// PEGA A INFO DAS EXCHANGES

class Functions
{
	function exchangeInfos($id,$location)
    {
        if($_SERVER['HTTP_HOST'] == 'coinscale.io')
        {
            $rootPath = $_SERVER['DOCUMENT_ROOT'];
        }
        else
        {
             $rootPath = 'https://coinscale.io';
        }

        if( $location == 'nacionais' )
        {
            $similarweb = json_decode(file_get_contents($rootPath.'/api/jsons/api-similarweb-nacionais.json'),true);
        }
        else
        {
            $similarweb = json_decode(file_get_contents($rootPath.'/api/jsons/api-similarweb-internacionais.json'),true);
        }

        $json = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/api/jsons/info_exchanges.json'), true);

        $exchange = $json[$id];

        $criptoFees = [];

        $tac = count($similarweb);
        $exchangeAccess = 0;

        for($ac = 1; $ac < $tac; $ac++)
        {
            if($similarweb[$ac]['id'] == $id)
            {
                $exchangeAccess = array(
                    'final_score'   => $similarweb[$ac]['nota'],
                    'total_access'  => number_format($similarweb[$ac]['acessos'],0,'.','.'),
                );
            }
        }

        if( isset($exchange['fee']) )
        {
            foreach ($exchange['fee'] as $key => $value)
            {
                array_push(
                    $criptoFees,
                    array(
                        $key => array(
                            'score'      => $value['final_score'],
                            'range'      => $value['final_range'],
                            'total_flow' => $value['total_flow'],
                        )
                    )
                );
            }
        }

        return array(
            'id'            => $exchange['id'],
            'location'      => $exchange['location'],
            'title'         => $exchange['titulo'],
            'nicename'      => $exchange['nicename'],
            'thumbnail'     => $exchange['thumbnail'],
            'imageFull'     => $exchange['imageFull'],
            'site'          => $exchange['site'],
            'reclameaqui'   => $exchange['reclameaqui'],
            'fee'           => $criptoFees,
            'access'        => array(  
                'access_data'   => ($exchangeAccess != 0) ? $exchangeAccess : null,
                'access_month'  => $similarweb[0],
            )
        );
	}
}