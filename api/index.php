<?php

require_once 'classes/class_render-endpoints-trading.php';
require_once 'classes/class_render-endpoints-fee.php';
require_once 'classes/class_render-endpoints-exchanges.php';
require_once 'classes/class_render-endpoints-access.php';

$endpointTrading 	= new RenderEndpointTrading();
$endpointFee 		= new RenderEndpointFee();
$endpointExchanges 	= new RenderEndpointExchanges();
$endpointaccess 	= new RenderEndpointAccess();

$route = explode('/',str_replace("/api/", "", $_SERVER['REQUEST_URI']));

if($_SERVER['HTTP_HOST'] == 'coinscale.io')
{
    $GLOBALS["rootPath"] = $_SERVER['DOCUMENT_ROOT'];
}
else
{
	$GLOBALS["rootPath"] = 'https://coinscale.io';
}

switch($route[0])
{
	case 'trading':
		$endpointTrading->RenderTradingEndpoint($route);
	break;

	case 'fee':
		$endpointFee->RenderFeeEndpoint($route);
	break;

	case 'exchanges':
		$endpointExchanges->RenderExchangeEndpoint($route);
	break;

	case 'access':
		$endpointaccess->RenderAccessEndpoint($route,'token','url');
	break;
	
	default:
		echo 'ROUTE ERROR!';
	break;
}